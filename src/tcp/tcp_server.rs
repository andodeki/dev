use std::net::SocketAddr;

use tracing::info;

use crate::configs::tcp::TcpConfig;
use crate::infrastructure::systems::system::SharedSystem;
use crate::tcp::tcp_listener;

/// Starts the TCP server.
/// Returns the address the server is listening on.
pub async fn start(config: TcpConfig, system: SharedSystem) -> SocketAddr {
    let server_name = if config.tls.enabled {
        "Iggy TCP TLS"
    } else {
        "Iggy TCP"
    };
    info!("Initializing {server_name} server...");
    let addr = match config.tls.enabled {
        // true => tcp_tls_listener::start(&config.address, config.tls, system).await,
        true => tcp_listener::start(&config.address, system).await,
        false => tcp_listener::start(&config.address, system).await,
    };
    info!("{server_name} server has started on: {:?}", addr);
    addr
}

// /// Starts the TCP server.
// /// Returns the address the server is listening on.
// pub async fn start(config: TcpConfig, system: SharedSystem) -> SocketAddr {
//     let server_name = if config.tls.enabled {
//         "Nigig TCP TLS"
//     } else {
//         "Nigig TCP"
//     };
//     info!("Initializing {server_name} server...");
//     // let addr: SocketAddr = match config.tls.enabled {
//     //     true => tcp_listener::start(&config.address, system).await;
//     //     false =>  tcp_listener::start(&config.address, system).await;
//     // };
//     let addr = match config.tls.enabled {
//         // true => tcp_tls_listener::start(&config.address, config.tls, system).await,
//         true => tcp_listener::start(&config.address, system).await,
//         false => tcp_listener::start(&config.address, system).await,
//     };
//     info!("{server_name} server has started on: {:?}", addr);
//     addr
// }

// pub async fn start2(config: TcpConfig, system: SharedSystem) -> SocketAddr {
//     let server_name = if config.tls.enabled {
//         "Iggy TCP TLS"
//     } else {
//         "Iggy TCP"
//     };
//     info!("Initializing {server_name} server...");
//     let addr = match config.tls.enabled {
//         true => {
//             // tcp_tls_listener::start(&config.address, config.tls, system).await
//             tcp_listener::start(&config, system).await;
//         }
//         false => {
//             // tcp_listener::start(&config.address, system).await
//             tcp_listener::start(&config, system).await;
//         }
//     };
//     info!("{server_name} server has started on: {:?}", addr);
//     addr
// }

// pub fn start(address: &str, system: SharedSystem) {
//     let address = address.to_string();
//     tokio::spawn(async move {
//         let listener = TcpListener::bind(address.clone()).await;
//         if listener.is_err() {
//             panic!("Unable to start TCP server on addr {}.", address);
//         }
//         let listener = listener.unwrap();
//         loop {
//             match listener.accept().await {
//                 Ok((stream, address)) => {
//                     info!("Accepted new TCP connection: {}", address);
//                     let system = system.clone();
//                     let mut sender = TcpSender { stream };
//                     tokio::spawn(async move {
//                         if let Err(error) =
//                             handle_connection(address, &mut sender, system.clone()).await
//                         {
//                             handle_error(error);
//                             system.read().await.delete_client(&address).await;
//                         }
//                     });
//                 }
//                 Err(error) => error!("Unable to accept TCP socket, error: {}", error),
//             }
//         }
//     });
// }
