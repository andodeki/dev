use crate::binary::command;
use crate::binary::sender::Sender;
use crate::infrastructure::clients::client_manager::Transport;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::command::Command;
use crate::server_error::ServerError;
// use iggy::bytes_serializable::BytesSerializable;
use bytes::{BufMut, BytesMut};
use serde::{Deserialize, Serialize};
use std::io::ErrorKind;
use std::net::SocketAddr;

// use std::net::SocketAddr;
use crate::models::bytes_serializable::BytesSerializable;
use tokio::io::{AsyncReadExt, AsyncWriteExt, BufReader};
use tokio::net::TcpStream;
use tracing::{debug, error, info, warn};

const INITIAL_BYTES_LENGTH: usize = 4;

pub(crate) async fn handle_conn(
    address: SocketAddr,
    sender: &mut dyn Sender,
    system: SharedSystem,
) -> Result<(), ServerError> {
    let client_id = system.read().add_client(&address, Transport::Tcp).await;

    let session = Session::from_client_id(client_id, address);
    let mut initial_buffer = [0u8; INITIAL_BYTES_LENGTH];
    loop {
        let read_length = sender.read(&mut initial_buffer).await?;
        if read_length != INITIAL_BYTES_LENGTH {
            return Err(ServerError::CommandLengthError(format!(
                "Unable to read the TCP request length, expected: {INITIAL_BYTES_LENGTH} bytes, received: {read_length} bytes."
            )));
        }

        let length = u32::from_le_bytes(initial_buffer);
        debug!("Received a TCP request, length: {length}");
        let mut command_buffer = BytesMut::with_capacity(length as usize);
        command_buffer.put_bytes(0, length as usize);
        sender.read(&mut command_buffer).await?;
        let command = Command::from_bytes(command_buffer.freeze())?;
        debug!("Received a TCP command: {command}, payload size: {length}");
        command::handle(&command, sender, &session, system.clone()).await?;
        debug!("Sent a TCP response.");
    }
}
pub(crate) fn handle_error(error: ServerError) {
    match error {
        ServerError::IoError(error) => match error.kind() {
            ErrorKind::UnexpectedEof => {
                info!("Connection has been closed.");
            }
            ErrorKind::ConnectionAborted => {
                info!("Connection has been aborted.");
            }
            ErrorKind::ConnectionRefused => {
                info!("Connection has been refused.");
            }
            ErrorKind::ConnectionReset => {
                info!("Connection has been reset.");
            }
            ErrorKind::InvalidInput => {
                info!("Invalid Input.");
            }
            ErrorKind::InvalidData => {
                info!("Invalid data.");
            }
            ErrorKind::Other => {
                info!("Connection has been other.");
            }
            ErrorKind::NotFound => {
                info!("Not Found.");
            }
            ErrorKind::PermissionDenied => {
                info!("Permission Denied.");
            }
            ErrorKind::NotConnected => {
                info!("Not Connected.");
            }
            ErrorKind::AddrInUse => {
                info!("Address in use.");
            }
            ErrorKind::AddrNotAvailable => {
                info!("Address not available.");
            }
            ErrorKind::BrokenPipe => {
                info!("Broken Pipe.");
            }
            ErrorKind::AlreadyExists => {
                info!("Address already exists.");
            }
            ErrorKind::WouldBlock => {
                info!("Would block.");
            }
            ErrorKind::TimedOut => {
                info!("Timed Out.");
            }
            ErrorKind::WriteZero => {
                info!("Write zero.");
            }
            ErrorKind::Interrupted => {
                info!("Connection interrupted.");
            }
            ErrorKind::Unsupported => {
                info!("Unsupported.");
            }
            ErrorKind::OutOfMemory => {
                info!("Out of Memory.");
            }
            // ErrorKind::StorageFull => todo!(),
            // ErrorKind::NotSeekable => todo!(),
            // ErrorKind::FilesystemQuotaExceeded => todo!(),
            // ErrorKind::FileTooLarge => todo!(),
            // ErrorKind::ResourceBusy => todo!(),
            // ErrorKind::ExecutableFileBusy => todo!(),
            // ErrorKind::Deadlock => todo!(),
            // ErrorKind::CrossesDevices => todo!(),
            // ErrorKind::TooManyLinks => todo!(),
            // ErrorKind::InvalidFilename => todo!(),
            // ErrorKind::ArgumentListTooLong => todo!(),
            // ErrorKind::NotADirectory => todo!(),
            // ErrorKind::IsADirectory => todo!(),
            // ErrorKind::DirectoryNotEmpty => todo!(),
            // ErrorKind::ReadOnlyFilesystem => todo!(),
            // ErrorKind::FilesystemLoop => todo!(),
            // ErrorKind::StaleNetworkFileHandle => todo!(),
            // ErrorKind::NetworkDown => todo!(),
            // ErrorKind::HostUnreachable => todo!(),
            // ErrorKind::NetworkUnreachable => todo!(),
            // _ => todo!(),
            _ => {
                error!("Connection has failed>>>: {error}");
            }
        },
        ServerError::SystemError(error) => {
            error!("Connection has failed: [[[[[{error}]]]]]");
        }
        _ => {
            error!("Connection has failed<<<<: {error}");
        }
    }
}

pub async fn handle_connection(mut socket: TcpStream) -> Result<(), ServerError> {
    let (read_stream, mut write_stream) = socket.split();
    let mut read_stream = BufReader::new(read_stream);

    let mut buffer = Vec::new();

    loop {
        let mut buf = vec![0; 1024]; // Read buffer
        let bytes_read = read_stream.read(&mut buf).await?;

        if bytes_read == 0 {
            log::warn!("EOF received");
            Response::default();
            break;
        }

        buffer.extend(&buf[..bytes_read]);

        // Check if the buffer contains a complete line
        while let Some(newline_position) = buffer.iter().position(|&x| x == b'\n') {
            let line = buffer.drain(..=newline_position).collect::<Vec<u8>>();

            // Convert the line to a UTF-8 string
            let data = String::from_utf8(line.clone()).unwrap(); // Handle potential errors in a safer way
            tracing::info!("[Incoming] - {}", data);

            let (response, close) = match Request::from_data(&line) {
                Ok(request) => (Response::new(is_prime(request.number())), false),
                Err(e) => {
                    warn!("Received a malformed request. Sending back a malformed response and closing connection: {:?}", e);
                    (Response::default(), true)
                }
            };

            let res_bytes = response.to_bytes();
            write_stream.write_all(&res_bytes).await?;
            write_stream.write_all(b"\n").await?;

            if close {
                break;
            }
        }
    }
    Ok(())
}
// pub(crate) async fn handle_connection(
//     address: SocketAddr,
//     // sender: &mut dyn Sender,
//     // system: SharedSystem,
// ) -> Result<(), ServerError> {
//     // let client_id = system
//     //     .read()
//     //     .await
//     //     .add_client(&address, Transport::Tcp)
//     //     .await;

//     // let mut session = Session::from_client_id(client_id, address);
//     let mut initial_buffer = [0u8; INITIAL_BYTES_LENGTH];
//     loop {
//         let read_length = sender.read(&mut initial_buffer).await?;
//         if read_length != INITIAL_BYTES_LENGTH {
//             error!(
//                 "Unable to read the TCP request length, expected: {INITIAL_BYTES_LENGTH} bytes, received: {read_length} bytes.",
//             );
//             continue;
//         }

//         let length = u32::from_le_bytes(initial_buffer);
//         debug!("Received a TCP request, length: {length}");
//         let mut command_buffer = vec![0u8; length as usize];
//         sender.read(&mut command_buffer).await?;
//         let command = Command::from_bytes(&command_buffer)?;
//         debug!("Received a TCP command: {command}, payload size: {length}");
//         let result = command::handle(&command, sender, &mut session, system.clone()).await;
//         if result.is_err() {
//             error!("Error when handling the TCP request: {:?}", result.err());
//             continue;
//         }
//         debug!("Sent a TCP response.");
//     }
// }

#[derive(Debug, Default, Deserialize)]
struct Request {
    method: String,
    number: f64,
}

#[derive(Debug)]
enum RequestError {
    InvalidMethod(String),
    InvalidNumber(f64),
    // InvalidUTF8,
    DeserializationError(serde_json::Error),
    Utf8Err(std::str::Utf8Error),
}

impl From<serde_json::Error> for RequestError {
    fn from(e: serde_json::Error) -> Self {
        Self::DeserializationError(e)
    }
}
impl From<std::str::Utf8Error> for RequestError {
    fn from(e: std::str::Utf8Error) -> Self {
        Self::Utf8Err(e)
    }
}

impl std::fmt::Display for RequestError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for RequestError {}

const VALID_METHOD: &str = "isPrime";

impl Request {
    // fn from_slice(data: &[u8]) -> Result<Self, RequestError> {
    //     let request: Self = serde_json::from_slice(data)?;
    //     request.validate()?;
    //     Ok(request)
    // }
    // fn from_str(data: &str) -> Result<Self, RequestError> {
    //     let request: Self = serde_json::from_str(data)?;
    //     request.validate()?;
    //     Ok(request)
    // }
    fn from_data(data: &[u8]) -> Result<Self, RequestError> {
        let data_str = std::str::from_utf8(data)?;
        let request: Self = serde_json::from_str(data_str)?;
        request.validate()?;
        Ok(request)
    }

    fn validate(&self) -> Result<(), RequestError> {
        use RequestError::*;
        if self.method != VALID_METHOD {
            return Err(InvalidMethod(self.method.clone()));
        }
        if self.number != (self.number as i64) as f64 {
            return Err(InvalidNumber(self.number));
        }

        Ok(())
    }

    fn number(&self) -> i64 {
        self.number as i64
    }
}

#[derive(Debug, Default, Serialize)]
struct Response {
    method: String,
    prime: bool,
}

impl Response {
    fn new(prime: bool) -> Self {
        Self {
            method: VALID_METHOD.into(),
            prime: prime,
        }
    }

    fn to_bytes(&self) -> Vec<u8> {
        serde_json::to_vec(self).unwrap()
    }
}

/// From https://docs.rs/primes/latest/src/primes/lib.rs.html
fn firstfac(x: i64) -> i64 {
    if x % 2 == 0 {
        return 2;
    };

    for n in (1..).map(|m| 2 * m + 1).take_while(|m| m * m <= x) {
        if x % n == 0 {
            return n;
        };
    }
    // No factor found. It must be prime.
    x
}

fn is_prime(n: i64) -> bool {
    if n <= 1 {
        return false;
    }
    firstfac(n) == n
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_request_deserialize() {
        let data = b"{\"method\":\"isPrime\",\"number\":42}";
        let request = Request::from_data(data).unwrap();

        assert_eq!(request.method, "isPrime");
        assert_eq!(request.number(), 42);
    }

    #[test]
    fn test_response_serialize() {
        let resp = Response::new(true);
        let data = resp.to_bytes();
        let expected = b"{\"method\":\"isPrime\",\"prime\":true}";

        assert_eq!(&data, expected);
    }

    #[test]
    fn test_is_prime_negative() {
        let number = -1;
        assert_eq!(is_prime(number), false);
    }

    #[test]
    fn test_is_prime_zero() {
        let number = 0;
        assert_eq!(is_prime(number), false);
    }

    #[test]
    fn test_is_prime_positive() {
        let number = 13;
        assert_eq!(is_prime(number), true);

        let number = 16;
        assert_eq!(is_prime(number), false);
    }
}
