use crate::configs::http::HttpConfig;
// use crate::http::jwt::middleware::jwt_auth;
use super::http_server::build_app_state;
use crate::http::axum_http::jwt::middleware::*;
use crate::http::axum_http::system;
use crate::http::axum_http::users;
use crate::infrastructure::systems::system::SharedSystem;
use axum::{middleware, Router};
use std::net::SocketAddr;

pub async fn start(config: HttpConfig, system: SharedSystem) -> SocketAddr {
    let api_name = if config.tls.enabled {
        "HTTP API (TLS)"
    } else {
        "HTTP API"
    };

    let app_state = build_app_state(&config, system).await;
    let app = Router::new()
        .merge(system::router(app_state.clone(), &config.metrics))
        // .merge(personal_access_tokens::router(app_state.clone()))
        .merge(users::router(app_state.clone()))
        // .merge(streams::router(app_state.clone()))
        // .merge(topics::router(app_state.clone()))
        // .merge(consumer_groups::router(app_state.clone()))
        // .merge(consumer_offsets::router(app_state.clone()))
        // .merge(partitions::router(app_state.clone()))
        // .merge(messages::router(app_state.clone()))
        .layer(middleware::from_fn_with_state(app_state.clone(), jwt_auth));

    tracing::info!("Started {api_name} on: {:?}", config.address[0].clone());

    let listener = tokio::net::TcpListener::bind(config.address[0].clone())
        .await
        .unwrap();
    let address = listener
        .local_addr()
        .expect("Failed to get local address for HTTP server");

    tokio::task::spawn(async move {
        axum::serve(
            listener,
            app.into_make_service_with_connect_info::<SocketAddr>(),
        )
        .await
        .expect("Failed to start HTTP server");
    });

    address
}
