// boilerplate to run in different modes
// cfg_if! {
//     if #[cfg(feature = "axum")] {
// use axum::extract::{Path, State};
use crate::configs::http::HttpMetricsConfig;
use crate::http::axum_http::jwt::json_web_token::Identity;
use crate::http::error::CustomError;
use axum::extract::State;
use axum::routing::get;
use axum::{Extension, Json, Router};
// use crate::http::mapper;
use crate::http::shared::AppState;
use crate::infrastructure::session::Session;
// use iggy::models::client_info::{ClientInfo, ClientInfoDetails};
use crate::models::stats::Stats;
use std::sync::Arc;

const NAME: &str = "Nigiginc HTTP\n";
const PONG: &str = "pong\n";

pub fn router(state: Arc<AppState>, metrics_config: &HttpMetricsConfig) -> Router {
    let mut router = Router::new()
        .route("/", get(|| async { NAME }))
        .route("/ping", get(|| async { PONG }))
        .route("/stats", get(get_stats));
    // .route("/clients", get(get_clients))
    // .route("/clients/:client_id", get(get_client));
    if metrics_config.enabled {
        router = router.route(&metrics_config.endpoint, get(get_metrics));
    }

    router.with_state(state)
}
async fn get_metrics(State(state): State<Arc<AppState>>) -> Result<String, CustomError> {
    let system = state.system.read();
    Ok(system.metrics.get_formatted_output())
}

async fn get_stats(
    State(state): State<Arc<AppState>>,
    Extension(identity): Extension<Identity>,
) -> Result<Json<Stats>, CustomError> {
    let system = state.system.read();
    let stats = system
        .get_stats(&Session::stateless(identity.user_id, identity.ip_address))
        .await?;
    Ok(Json(stats))
}

// async fn get_client(
//     State(state): State<Arc<AppState>>,
//     Extension(identity): Extension<Identity>,
//     Path(client_id): Path<u32>,
// ) -> Result<Json<ClientInfoDetails>, CustomError> {
//     let system = state.system.read();
//     let client = system
//         .get_client(
//             &Session::stateless(identity.user_id, identity.ip_address),
//             client_id,
//         )
//         .await?;
//     let client = client.read().await;
//     let client = mapper::map_client(&client).await;
//     Ok(Json(client))
// }

// async fn get_clients(
//     State(state): State<Arc<AppState>>,
//     Extension(identity): Extension<Identity>,
// ) -> Result<Json<Vec<ClientInfo>>, CustomError> {
//     let system = state.system.read();
//     let clients = system
//         .get_clients(&Session::stateless(identity.user_id, identity.ip_address))
//         .await?;
//     let clients = mapper::map_clients(&clients).await;
//     Ok(Json(clients))
// }

//     } else {

//     }
// }
