use crate::http::shared::AppState;
use crate::http::shared::RequestDetails;
use crate::infrastructure::utils::random_id;
use std::borrow::Borrow;
use tokio::time::Instant;
use tracing::debug;
use xitca_web::http::WebResponse;
use xitca_web::middleware::sync::Next;
use xitca_web::WebContext;

pub fn request_diagnostics<E, C>(
    // ext: &RequestExt<()>,
    next: &mut Next<E>,
    mut ctx: WebContext<'_, C>,
) -> Result<WebResponse<()>, E>
where
    // S: for<'r> Service<WebContext<'r, C>, Response = WebResponse, Error = Error<C>>,
    C: Borrow<AppState>, // annotate we want to borrow &String from generic C state type.
{
    // ctx.state().borrow().system.read().metrics.increment_http_requests();
    let request_id = random_id::get_ulid();
    let ip_address = *ctx.req().body().socket_addr();
    let path_and_query = ctx
        .req()
        .uri()
        .path_and_query()
        .map(|p| p.as_str())
        .unwrap_or("/");
    debug!(
        "Processing a request {} {} with ID: {request_id} from client with IP address: {ip_address:?}...",
        ctx.req().method(),
        path_and_query,
    );
    ctx.req_mut().extensions_mut().insert(RequestDetails {
        request_id,
        ip_address,
    });
    let now = Instant::now();
    let result = next.call(ctx);
    let elapsed = now.elapsed();
    debug!(
        "Processed a request with ID: {request_id} from client with IP address: {ip_address:?} in {} ms.",
        elapsed.as_millis()
    );
    result
    // next.call(ctx).map(|res| {
    //     tracing::info!("metricx: response status: {}", res.status());
    //     res
    // })
}
