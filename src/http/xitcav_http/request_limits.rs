use std::{
    collections::{HashMap, HashSet},
    fmt,
    net::{IpAddr, SocketAddr},
    sync::{Arc, Mutex},
    time::{Duration, Instant, SystemTime},
};

use xitca_http::http::{HeaderValue, StatusCode};
use xitca_io::net::Stream;
use xitca_web::{
    error::Error,
    handler::Responder,
    http::header::{HeaderMap, HeaderName},
    http::WebResponse,
    service::{Service, ServiceExt},
    WebContext,
};

const X_FORWARDED_FOR: &str = "x-forwarded-for";

pub async fn request_limit<S, C>(
    service: &S,
    ctx: WebContext<'_, C>,
) -> Result<WebResponse, Error<C>>
where
    S: for<'r> Service<WebContext<'r, C>, Response = WebResponse, Error = Error<C>>,
{
    let addr = get_client_addr(ctx.req().headers());
    // let addr = *ctx.req().body().socket_addr();

    // rate limit based on client addr
    if check_addr(&addr) {
        return StatusCode::TOO_MANY_REQUESTS.respond(ctx).await;
    }

    service.call(ctx).await
}

pub async fn connection_limit<S>(service: &S, conn: Stream) -> Result<S::Response, S::Error>
where
    S: Service<Stream, Response = ()>,
{
    match &conn {
        Stream::Tcp(_, addr) => {
            // drop connection on condition.
            if check_addr(&addr) {
                return Ok(());
            }

            // delay handling on condition.
            if check_addr(&addr) {
                tokio::time::sleep(std::time::Duration::from_secs(1)).await;
            }
        }
        _ => {}
    }

    service.call(conn).await
}

// arbitrary function for checking client address
// fn check_addr(_: &std::net::SocketAddr) -> bool {
//     true
// }
fn check_addr(addr: &SocketAddr) -> bool {
    let rate_limiter = Arc::new(Mutex::new(Server::from_token()));
    rate_limiter.lock().unwrap().client_connected(*addr);
    if rate_limiter.lock().unwrap().client_read(*addr) {
        // tracing::info!("Request from {} allowed h", addr);
        rate_limiter.lock().unwrap().update(*addr);
        false
    } else {
        tracing::info!("Request from {} blocked due to rate limit", addr);
        // rate_limiter.lock().unwrap().update(*addr);
        true
    }
}

fn get_client_addr(headers: &HeaderMap) -> SocketAddr {
    if let Some(header_value) = headers.get(&HeaderName::from_static(X_FORWARDED_FOR)) {
        if let Ok(addr) = header_value.to_str() {
            if let Ok(parsed_addr) = addr.parse() {
                // tracing::info!("Request from {} allowed", addr);
                return parsed_addr;
            }
        }
    }
    SocketAddr::new(
        std::net::IpAddr::V4(std::net::Ipv4Addr::new(127, 0, 0, 1)),
        0,
    )
}

// Add these constants to define the rate limit and the duration for which a user is banned after exceeding the limit.
// const MESSAGE_RATE_LIMIT: Duration = Duration::from_secs(1);
// const RATE_LIMIT_STRIKE_LIMIT: i32 = 5;
// type CResult<T> = std::result::Result<T, ()>;
const SAFE_MODE: bool = false;
const BAN_LIMIT: Duration = Duration::from_secs(1 * 6);
const MESSAGE_RATE: Duration = Duration::from_secs(1);
const SLOWLORIS_LIMIT: Duration = Duration::from_millis(200);
const STRIKE_LIMIT: usize = 10;
struct Sens<T>(T);

impl<T: fmt::Display> fmt::Display for Sens<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self(inner) = self;
        if SAFE_MODE {
            "[REDACTED]".fmt(f)
        } else {
            inner.fmt(f)
        }
    }
}
struct Client {
    // conn: TcpStream,
    last_message: SystemTime,
    connected_at: SystemTime,
    // authed: bool,
    addr: SocketAddr,
}
enum Sinner {
    Striked(usize),
    Banned(SystemTime),
}

impl Sinner {
    fn new() -> Self {
        Self::Striked(0)
    }

    fn forgive(&mut self) {
        *self = Self::Striked(0)
    }

    fn strike(&mut self) -> bool {
        match self {
            Self::Striked(x) => {
                if *x >= STRIKE_LIMIT {
                    *self = Self::Banned(SystemTime::now());
                    true
                } else {
                    *x += 1;
                    false
                }
            }
            Self::Banned(_) => true,
        }
    }
}
struct Server {
    clients: HashMap<SocketAddr, Client>,
    sinners: HashMap<IpAddr, Sinner>,
    // token: String,
}

impl Server {
    fn from_token() -> Self {
        Self {
            clients: HashMap::new(),
            sinners: HashMap::new(),
            // token,
        }
    }
    fn client_connected(&mut self, author_addr: SocketAddr) {
        let now = SystemTime::now();

        if let Some(sinner) = self.sinners.get_mut(&author_addr.ip()) {
            match sinner {
                Sinner::Banned(banned_at) => {
                    let diff = now.duration_since(*banned_at).unwrap_or_else(|err| {
                            eprintln!("ERROR: ban time check on client connection: the clock might have gone backwards: {err}");
                            Duration::ZERO
                        });
                    if diff < BAN_LIMIT {
                        let secs = (BAN_LIMIT - diff).as_secs_f32();
                        // TODO: probably remove this logging, cause banned MFs may still keep connecting and overflow us with logs
                        println!("INFO: Client {author_addr} tried to connected, but that MF is banned for {secs} secs", author_addr = Sens(author_addr));

                        return;
                    } else {
                        sinner.forgive()
                    }
                }
                Sinner::Striked(_) => {}
            }
        }
        println!(
            "INFO: Client {author_addr} connected",
            author_addr = Sens(author_addr)
        );
        self.clients.insert(
            author_addr.clone(),
            Client {
                // conn: author,
                last_message: now - 2 * MESSAGE_RATE,
                connected_at: now,
                // authed: false,
                addr: author_addr,
            },
        );
    }
    // fn new_message(&mut self, author_addr: SocketAddr) -> bool {
    //     if let Some(author) = self.clients.get_mut(&author_addr) {
    //         let now = SystemTime::now();
    //         let diff = now.duration_since(author.last_message).unwrap_or_else(|err| {
    //             eprintln!("ERROR: message rate check on new message: the clock might have gone backwards: {err}");
    //             Duration::from_secs(0)
    //         });

    //         if diff >= MESSAGE_RATE {
    //             // No need to increment the strike count if the message rate is allowed
    //             tracing::info!(
    //                 "Current rate count {}/{} from address {}",
    //                 author.strike_count,
    //                 STRIKE_LIMIT,
    //                 author_addr
    //             );
    //             true
    //         } else {
    //             author.strike_count += 1;
    //             if author.strike_count >= STRIKE_LIMIT {
    //                 println!(
    //                     "INFO: Client {author_addr} got banned",
    //                     author_addr = Sens(author_addr)
    //                 );
    //                 tracing::info!(
    //                     "Current rate count {}/{} from address {}",
    //                     author.strike_count,
    //                     STRIKE_LIMIT,
    //                     author_addr
    //                 );
    //                 self.banned_mfs.insert(author_addr.ip().clone(), now);
    //                 self.clients.remove(&author_addr);
    //                 false
    //             } else {
    //                 // Incremented the strike count, but not yet banned
    //                 tracing::info!(
    //                     "Current rate count {}/{} from address {}",
    //                     author.strike_count,
    //                     STRIKE_LIMIT,
    //                     author_addr
    //                 );
    //                 false
    //             }
    //         }
    //     } else {
    //         // The client is not in the clients map
    //         true
    //     }
    // }
    fn strike_ip(&mut self, ip: IpAddr) {
        let sinner = self.sinners.entry(ip).or_insert(Sinner::new());
        if sinner.strike() {
            println!("INFO: IP {ip} got banned", ip = Sens(ip));
            self.clients.retain(|_token, client| {
                let addr: SocketAddr = client.addr.clone();
                if addr.ip() == ip {
                    return false;
                }
                true
            });
        }
    }
    fn update(&mut self, author_addr: SocketAddr) {
        self.client_read(author_addr);

        // TODO: keep waiting connections in a separate hash map
        self.clients.retain(|_, client| {
                let addr: SocketAddr = client.addr.clone();
                // if !client.authed {
                    let now = SystemTime::now();
                    let diff = now.duration_since(client.connected_at).unwrap_or_else(|err| {
                        eprintln!("ERROR: slowloris time limit check: the clock might have gone backwards: {err}");
                        SLOWLORIS_LIMIT
                    });
                    if diff >= SLOWLORIS_LIMIT {
                        // TODO: disconnect everyone from addr.ip()
                        self.sinners.entry(addr.ip()).or_insert(Sinner::new()).strike();
                        return false;
                    }
                // }
                true
            });
    }
    fn client_read(&mut self, sauthor_addr: SocketAddr) -> bool {
        if let Some(author) = self.clients.get_mut(&sauthor_addr) {
            let author_addr: SocketAddr = author.addr.clone();
            let now = SystemTime::now();
            let diff = now.duration_since(author.last_message).unwrap_or_else(|err| {
                eprintln!("ERROR: message rate check on new message: the clock might have gone backwards: {err}");
                Duration::from_secs(0)
            });
            if diff < MESSAGE_RATE {
                self.strike_ip(author_addr.ip());
                return false;
            }
            tracing::info!(
                "Current rate count {:?}/{:?} from address {}",
                diff,
                MESSAGE_RATE,
                author_addr
            );
            self.sinners
                .entry(author_addr.ip())
                .or_insert(Sinner::new())
                .forgive();
            author.last_message = now;
            // TODO: let the user know that they were banned after this attempt
            self.clients.remove(&sauthor_addr);
            // TODO: each IP strike must be properly documented in the source code giving the reasoning
            // behind it.
            self.strike_ip(author_addr.ip());
            // author.authed = true;
            println!("INFO: {} authorized!", Sens(author_addr));
            true
        } else {
            // Handle the case where the client is not in the clients map
            false
        }
    }
}
// fn client(addr: &SocketAddr) -> CResult<bool> {
//     let mut server = Server::from_token();
//     Ok(server.client_read(*addr))
// }
