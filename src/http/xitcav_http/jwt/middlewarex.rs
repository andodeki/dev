use crate::http::axum_http::jwt::json_web_token::Identity;
use crate::http::shared::{AppState, RequestDetails};

use std::borrow::Borrow;
use xitca_http::http::StatusCode;
use xitca_service::Service;
use xitca_web::error::Error;
// use xitca_web::handler::state::StateRef;
// use xitca_web::handler::FromRequest;
// use xitca_web::http::WebResponse;
use xitca_web::WebContext;

const AUTHORIZATION: &str = "authorization";
pub const BEARER: &str = "Bearer ";
const UNAUTHORIZED: StatusCode = StatusCode::UNAUTHORIZED;

const UNAUTHORIZED_PATHS: &[&str] = &[
    "/",
    "/metrics",
    "/ping",
    "/users/login",
    "/users/refresh-token",
    "/personal-access-tokens/login",
];
pub async fn middleware_fn<S, C, Res>(
    service: &S,
    mut ctx: WebContext<'_, C>,
) -> Result<Res, Error<C>>
where
    S: for<'r> Service<WebContext<'r, C>, Response = Res, Error = Error<C>>,
    C: Borrow<AppState>, // annotate we want to borrow &String from generic C state type.
{
    if UNAUTHORIZED_PATHS.contains(&ctx.req().uri().path()) {
        return service.call(ctx).await;
    }
    let bearer = ctx
        .req()
        .headers()
        .get(AUTHORIZATION)
        .ok_or(UNAUTHORIZED)?
        .to_str()
        .map_err(|_| UNAUTHORIZED)?;

    if !bearer.starts_with(BEARER) {
        return Err(StatusCode::UNAUTHORIZED.into());
    }
    let jwt_token = &bearer[BEARER.len()..];
    let token_header = jsonwebtoken::decode_header(jwt_token).map_err(|_| UNAUTHORIZED)?;
    let jwt_claims = ctx
        .state()
        .borrow()
        .jwt_manager
        .decode(jwt_token, token_header.alg)
        .map_err(|_| UNAUTHORIZED)?;
    if ctx
        .state()
        .borrow()
        .jwt_manager
        .is_token_revoked(&jwt_claims.claims.jti)
        .await
    {
        return Err(StatusCode::UNAUTHORIZED.into());
    }
    let request_details = ctx.req().extensions().get::<RequestDetails>().unwrap();
    let identity = Identity {
        token_id: jwt_claims.claims.jti,
        token_expiry: jwt_claims.claims.exp,
        user_id: jwt_claims.claims.sub,
        ip_address: request_details.ip_address,
    };
    ctx.req_mut().extensions_mut().insert(identity);
    // // WebContext::state would return &C then we can call Borrow::borrow on it to get &String
    // let _appstate = ctx.state().borrow();
    // // or use extractor manually like in function service.
    // let _appstate = StateRef::<'_, AppState>::from_request(&ctx).await?;
    // // service.call(ctx).await
    service.call(ctx).await.map(|res| {
        // tracing::info!("middleware_fn: response status: ");
        res
    })
}
