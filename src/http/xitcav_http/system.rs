use crate::configs::http::HttpMetricsConfig;
use crate::http::axum_http::jwt::json_web_token::Identity;
use crate::http::error::CustomError;
use std::{convert::Infallible, sync::Arc};
// use crate::http::mapper;
use crate::http::shared::AppState;
use crate::infrastructure::session::Session;
// use iggy::models::client_info::{ClientInfo, ClientInfoDetails};
use crate::models::stats::Stats;
use xitca_http::util::service::route::{get, post};
use xitca_http::{
    h1, h3,
    http::{
        const_header_value::TEXT_UTF8, header::CONTENT_TYPE, Request, RequestExt, Response, Version,
    },
    ResponseBody,
};
use xitca_web::handler::extension::ExtensionRef;
use xitca_web::handler::handler_service;
use xitca_web::handler::json::Json;
use xitca_web::handler::state::StateRef;
use xitca_web::NestApp;

// use super::http_server::error::CustomError;
// use xitca_service::{fn_service, object, Service, ServiceExt};

pub const NAME: &str = "Nigiginc HTTP\n";
pub const PONG: &str = "pong\n";

// pub(super) fn routes<C: 'static + Borrow<AppState>>(
//     app: NestApp<C>,
//     // state: Arc<AppState>,
//     metrics_config: &HttpMetricsConfig,
// ) -> NestApp<C> {
//     let mut app = app
//         .at("/", get(handler_service(|| async { NAME })))
//         .at("/ping", get(handler_service(|| async { PONG })))
//         .at("/stats", get(handler_service(get_stats)));

//     if metrics_config.enabled {
//         app = app.at(&metrics_config.endpoint, get(handler_service(get_metrics)));
//     }
//     app
// }

// pub fn app() -> NestApp<usize> {
//     App::new()
//         .at(
//             "/index",
//             handler_service(|_: &WebContext<'_, usize>| async { "Test\n" }),
//         )
//         .at("/", get(handler_service(|| async { NAME })))
//         .at("/", get(handler_service(|| async { PONG })))
//         // .at("/stats", get(handler_service(get_stats)))
//         .at("/metrics", get(handler_service(get_metrics)))
// }
// pub(super) fn route<C: 'static>(app: NestApp<C>) -> NestApp<C> {
//     app.at("/", get(handler_service(|| async { NAME })))
//         .at("/ping", get(handler_service(|| async { PONG })))
//         // .at("/stats", get(handler_service(get_stats)))
//         .at("/metrics", get(handler_service(get_metrics)))
// }
pub(super) fn routes(app: NestApp<Arc<AppState>>) -> NestApp<Arc<AppState>> {
    app.at("/", get(handler_service(|| async { NAME })))
        .at("/ping", get(handler_service(|| async { PONG })))
        .at("/metrics", get(handler_service(get_metrics)))
        .at("/stats", get(handler_service(get_stats)))
}
pub async fn get_metrics(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    // ExtensionRef(state): ExtensionRef<'_, Arc<AppState>>>,
    // ctx: &WebContext<'_, Arc<AppState>>>,
) -> Result<String, CustomError> {
    // let system = ctx.state().system.read();
    let system = state.system.read();
    Ok(system.metrics.get_formatted_output())
}
// pub fn app<C: 'static>() -> NestApp<C> {
//     App::new().at(
//         "/index",
//         handler_service(|_: &WebContext<'_, usize>| async { NAME }),
//     )
// }

pub async fn get_stats(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
) -> Result<Json<Stats>, CustomError> {
    let system = state.system.read();
    let stats = system
        .get_stats(&Session::stateless(identity.user_id, identity.ip_address))
        .await?;
    Ok(Json(stats))
}
pub fn router_h3(state: Arc<AppState>, metrics_config: &HttpMetricsConfig) {
    // ) -> Router<object::ServiceObject> {
    // let mut router = Router::new();
    //     .insert(
    //     "/",
    //     get(fn_service(handler_h3).enclosed(
    //         // a show case of nested enclosed middleware
    //         Group::new()
    //             .enclosed(HttpServiceBuilder::h3())
    //             .enclosed(Logger::default()),
    //     )),
    // );
    // .route("/", get(|| async { NAME }))
    // .route("/ping", get(|| async { PONG }));
    // .route("/stats", get(get_stats));
    // .route("/clients", get(get_clients))
    // .route("/clients/:client_id", get(get_client));
    // if metrics_config.enabled {
    //     router = router.route(&metrics_config.endpoint, get(get_metrics));
}

async fn handler_h1(
    _: Request<RequestExt<h1::RequestBody>>,
) -> Result<Response<ResponseBody>, Infallible> {
    Ok(Response::builder()
        .header(CONTENT_TYPE, TEXT_UTF8)
        .body("Hello World from Http/1!".into())
        .unwrap())
}
async fn handler_h3(
    _: Request<RequestExt<h3::RequestBody>>,
) -> Result<Response<ResponseBody>, Box<dyn std::error::Error>> {
    Response::builder()
        .status(200)
        .version(Version::HTTP_3)
        .header(CONTENT_TYPE, TEXT_UTF8)
        .body("Hello World from Http/3!".into())
        .map_err(Into::into)
}
