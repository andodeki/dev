use std::sync::Arc;

use crate::http::axum_http::jwt::json_web_token::Identity;
use crate::http::error::CustomError;
use crate::http::mapper;
use crate::http::mapper::map_generated_tokens_to_identity_info;
use crate::http::shared::AppState;
use crate::infrastructure::session::Session;
use crate::models::identifier::Identifier;
use crate::models::identity_info::IdentityInfo;
use crate::models::user_info::{UserInfo, UserInfoDetails};
use crate::models::users::change_password::ChangePassword;
use crate::models::users::create_user::CreateUser;
use crate::models::users::login_user::LoginUser;
use crate::models::users::logout_user::LogoutUser;
use crate::models::users::update_permissions::UpdatePermissions;
use crate::models::users::update_user::UpdateUser;
use crate::models::validatable::Validatable;
// use axum::extract::{Path, State};
use xitca_http::http::StatusCode;
// use axum::routing::{get, post, put};
// use axum::{Extension, Json, Router};
use serde::Deserialize;
use xitca_web::handler::extension::ExtensionRef;
use xitca_web::handler::handler_service;
use xitca_web::handler::json::Json;
use xitca_web::handler::path::PathRef;
use xitca_web::handler::state::StateRef;
use xitca_web::route::{get, post, put};
use xitca_web::NestApp;

// pub fn router(state: Arc<AppState>) -> Router {
//     Router::new()
//         .route("/users", get(get_users).post(create_user))
//         .route(
//             "/:user_id",
//             get(get_user).put(update_user).delete(delete_user),
//         )
//         .route("/:user_id/permissions", put(update_permissions))
//         .route("/:user_id/password", put(change_password))
//         .route("/login", post(login_user))
//         .route("/logout", post(logout_user))
//         .route("/refresh-token", post(refresh_token))
//         .with_state(state)
// }
pub(super) fn routes(app: NestApp<Arc<AppState>>) -> NestApp<Arc<AppState>> {
    app.at(
        "/users",
        get(handler_service(get_users)).post(handler_service(create_user)),
    )
    .at(
        "/:user_id",
        get(handler_service(get_user))
            .put(handler_service(update_user))
            .delete(handler_service(delete_user)),
    )
    .at(
        "/:user_id/permissions",
        put(handler_service(update_permissions)),
    )
    .at("/:user_id/password", put(handler_service(change_password)))
    .at("/login", post(handler_service(login_user)))
    .at("/logout", post(handler_service(logout_user)))
    .at("/refresh-token", post(handler_service(refresh_token)))
}

pub async fn get_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    PathRef(user_id): PathRef<'_>,
) -> Result<Json<UserInfoDetails>, CustomError> {
    let user_id = Identifier::from_str_value(&user_id)?;
    let system = state.system.read();
    let user = system
        .find_user(
            &Session::stateless(identity.user_id, identity.ip_address),
            &user_id,
        )
        .await?;
    let user = mapper::map_user(&user);
    Ok(Json(user))
}

pub async fn get_users(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
) -> Result<Json<Vec<UserInfo>>, CustomError> {
    let system = state.system.read();
    let users = system
        .get_users(&Session::stateless(identity.user_id, identity.ip_address))
        .await?;
    let users = mapper::map_users(&users);
    Ok(Json(users))
}

pub async fn create_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    Json(command): Json<CreateUser>,
) -> Result<StatusCode, CustomError> {
    command.validate()?;
    let mut system = state.system.write();
    system
        .create_user(
            &Session::stateless(identity.user_id, identity.ip_address),
            &command.username,
            &command.password,
            command.status,
            command.permissions.clone(),
        )
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn update_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    PathRef(user_id): PathRef<'_>,
    Json(mut command): Json<UpdateUser>,
) -> Result<StatusCode, CustomError> {
    command.user_id = Identifier::from_str_value(&user_id)?;
    command.validate()?;
    let system = state.system.read();
    system
        .update_user(
            &Session::stateless(identity.user_id, identity.ip_address),
            &command.user_id,
            command.username,
            command.status,
        )
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn update_permissions(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    PathRef(user_id): PathRef<'_>,
    Json(mut command): Json<UpdatePermissions>,
) -> Result<StatusCode, CustomError> {
    command.user_id = Identifier::from_str_value(&user_id)?;
    command.validate()?;
    let mut system = state.system.write();
    system
        .update_permissions(
            &Session::stateless(identity.user_id, identity.ip_address),
            &command.user_id,
            command.permissions,
        )
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn change_password(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    PathRef(user_id): PathRef<'_>,
    Json(mut command): Json<ChangePassword>,
) -> Result<StatusCode, CustomError> {
    command.user_id = Identifier::from_str_value(&user_id)?;
    command.validate()?;
    let system = state.system.read();
    system
        .change_password(
            &Session::stateless(identity.user_id, identity.ip_address),
            &command.user_id,
            &command.current_password,
            &command.new_password,
        )
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn delete_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    PathRef(user_id): PathRef<'_>,
) -> Result<StatusCode, CustomError> {
    let user_id = Identifier::from_str_value(&user_id)?;
    let mut system = state.system.write();
    system
        .delete_user(
            &Session::stateless(identity.user_id, identity.ip_address),
            &user_id,
        )
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn login_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    Json(command): Json<LoginUser>,
) -> Result<Json<IdentityInfo>, CustomError> {
    command.validate()?;
    let system = state.system.read();
    let user = system
        .login_user(&command.username, &command.password, None)
        .await?;
    let tokens = state.jwt_manager.generate(user.id)?;
    Ok(Json(map_generated_tokens_to_identity_info(tokens)))
}

pub async fn logout_user(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    ExtensionRef(identity): ExtensionRef<'_, Identity>,
    Json(command): Json<LogoutUser>,
) -> Result<StatusCode, CustomError> {
    command.validate()?;
    let system = state.system.read();
    system
        .logout_user(&Session::stateless(identity.user_id, identity.ip_address))
        .await?;
    state
        .jwt_manager
        .revoke_token(&identity.token_id, identity.token_expiry)
        .await?;
    Ok(StatusCode::NO_CONTENT)
}

pub async fn refresh_token(
    StateRef(state): StateRef<'_, Arc<AppState>>,
    Json(command): Json<RefreshToken>,
) -> Result<Json<IdentityInfo>, CustomError> {
    let tokens = state.jwt_manager.refresh_token(&command.refresh_token)?;
    Ok(Json(map_generated_tokens_to_identity_info(tokens)))
}

#[derive(Debug, Deserialize)]
pub struct RefreshToken {
    pub refresh_token: String,
}
