use crate::http::shared::AppState;
use std::borrow::Borrow;
use xitca_web::http::WebResponse;
use xitca_web::middleware::sync::Next;
use xitca_web::WebContext;

pub fn metricsx<E, C>(next: &mut Next<E>, ctx: WebContext<'_, C>) -> Result<WebResponse<()>, E>
where
    // S: for<'r> Service<WebContext<'r, C>, Response = WebResponse, Error = Error<C>>,
    C: Borrow<AppState>, // annotate we want to borrow &String from generic C state type.
{
    ctx.state()
        .borrow()
        .system
        .read()
        .metrics
        .increment_http_requests();
    next.call(ctx).map(|res| {
        // tracing::info!("metricx: response status: {}", res.status());
        res
    })
}
