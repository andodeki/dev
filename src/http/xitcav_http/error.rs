use crate::infrastructure::error::Error as InfraError;
use std::{convert::Infallible, error, fmt};
// use thiserror::Error as TError;

// use xitca_http::http::IntoResponse;
use xitca_web::codegen::error_impl;
use xitca_web::{error::Error, http::WebResponse, service::Service, WebContext};

// #[derive(Debug)]
// pub enum XitcaCustomError {
//     // NigigServerError(InfraError),
//     NigigServerError,
// }
#[derive(Debug, thiserror::Error)]
pub enum XitcaCustomError {
    #[error(transparent)]
    NigigServerError(#[from] InfraError),
    // NigigServerError(#[from] InfraError),
}

// Error<C> is the main error type xitca-web uses and at some point XitcaCustomError would
// need to be converted to it.
impl<C> From<XitcaCustomError> for Error<C> {
    fn from(e: XitcaCustomError) -> Self {
        Error::from_service(e)
    }
}
// #[error_impl]
// impl XitcaCustomError {
//     async fn call<C>(&self, ctx: WebContext<'_, C>) -> WebResponse {
//         // logic of generating a response from your error.
//     }
// }

// response generator of XitcaCustomError. in this case we generate blank bad request error.
impl<'r, C> Service<WebContext<'r, C>> for XitcaCustomError {
    type Response = WebResponse;
    type Error = Infallible;

    async fn call(&self, ctx: WebContext<'r, C>) -> Result<Self::Response, Self::Error> {
        xitca_web::error::BadRequest.call(ctx).await
    }
}

// a middleware function used for intercept and interact with app handler outputs.
pub async fn error_handler<S, C, Res>(s: &S, ctx: WebContext<'_, C>) -> Result<Res, Error<C>>
where
    S: for<'r> Service<WebContext<'r, C>, Response = Res, Error = Error<C>>,
{
    s.call(ctx).await.map_err(|e| {
        // debug format error info.
        println!("{e:?}");

        // display format error info.
        println!("{e}");

        // utilize std::error::Error trait methods for backtrace and more advanced error info.
        let _source = e.source();

        // // upcast trait and downcast to concrete type again.
        // // this offers the ability to regain typed error specific error handling.
        // // *. this is a runtime feature and not reinforced at compile time.
        // if let Some(e) = (&*e as &dyn error::Error).downcast_ref::<XitcaCustomError>() {
        //     match e {
        //         XitcaCustomError::NigigServerError => {}
        //     }
        // }

        e
    })
}
