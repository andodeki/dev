use crate::configs::http::{HttpConfig, HttpCorsConfig};
// use crate::http::diagnostics::request_diagnostics;
use crate::http::axum_http::jwt::jwt_manager::JwtManager;
// use crate::http::metrics::metrics;
use crate::http::error::error_handler;
use crate::http::shared::AppState;
use crate::http::xitcav_http::diagnostics::request_diagnostics;
use crate::http::xitcav_http::jwt::middlewarex::middleware_fn;
use crate::http::xitcav_http::metrics::metricsx;
use crate::http::xitcav_http::request_limits::{connection_limit, request_limit};
use crate::http::xitcav_http::{system, users};
use crate::infrastructure::systems::system::SharedSystem;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::{convert::Infallible, io, sync::Arc};
use xitca_web::middleware::rate_limit::RateLimit;

// use axum::http::Method;
// use axum::{middleware, Router};
use std::net::SocketAddr;
use tower_http::{
    cors::{AllowOrigin, CorsLayer},
    // set_status::SetStatusLayer,
};
use tracing::info;
use xitca_http::{
    // http,
    util::service::{
        route::{get, post, Route},
        router::{Router, RouterError},
    },
};
use xitca_web::middleware::Extension;

use openssl::ssl::{AlpnError, SslAcceptor, SslFiletype, SslMethod};
use quinn::ServerConfig;
use rustls::{Certificate, PrivateKey};
use xitca_http::{
    h1,
    h2,
    h3,
    http::{
        const_header_value::TEXT_UTF8, header::CONTENT_TYPE, Method, Request, RequestExt, Response,
        Version,
    },
    // middleware::tower_http_compat::TowerHttpCompat as CompatMiddleware,
    util::middleware::{Logger, SocketConfig},
    HttpServiceBuilder,
    ResponseBody,
};
use xitca_server::ServerFuture;
use xitca_service::{fn_service, middleware, ServiceExt};
use xitca_web::handler::handler_service;
use xitca_web::middleware::sync::SyncMiddleware;
use xitca_web::middleware::{tower_http_compat::TowerHttpCompat, Group};
use xitca_web::{App, WebContext};

fn configure_cors(config: HttpCorsConfig) -> CorsLayer {
    let allowed_origins = match config.allowed_origins {
        origins if origins.is_empty() => AllowOrigin::default(),
        origins if origins.first().unwrap() == "*" => AllowOrigin::any(),
        origins => AllowOrigin::list(origins.iter().map(|s| s.parse().unwrap())),
    };

    let allowed_headers = config
        .allowed_headers
        .iter()
        .map(|s| s.parse().unwrap())
        .collect::<Vec<_>>();

    let exposed_headers = config
        .exposed_headers
        .iter()
        .map(|s| s.parse().unwrap())
        .collect::<Vec<_>>();

    let allowed_methods = config
        .allowed_methods
        .iter()
        .map(|s| match s.to_uppercase().as_str() {
            "GET" => Method::GET,
            "POST" => Method::POST,
            "PUT" => Method::PUT,
            "DELETE" => Method::DELETE,
            "HEAD" => Method::HEAD,
            "OPTIONS" => Method::OPTIONS,
            "CONNECT" => Method::CONNECT,
            "PATCH" => Method::PATCH,
            "TRACE" => Method::TRACE,
            _ => panic!("Invalid HTTP method: {}", s),
        })
        .collect::<Vec<_>>();

    CorsLayer::new()
        .allow_methods(allowed_methods)
        .allow_origin(allowed_origins)
        .allow_headers(allowed_headers)
        .expose_headers(exposed_headers)
        .allow_credentials(config.allow_credentials)
        .allow_private_network(config.allow_private_network)
}

pub async fn build_app_state(config: &HttpConfig, system: SharedSystem) -> Arc<AppState> {
    let db;
    {
        let system_read = system.read();
        db = system_read
            .db
            .as_ref()
            .expect("Database not initialized")
            .clone();
    }

    let jwt_manager = JwtManager::from_config(&config.jwt, db);
    if let Err(error) = jwt_manager {
        panic!("Failed to initialize JWT manager: {}", error);
    }

    let jwt_manager = jwt_manager.unwrap();
    if jwt_manager.load_revoked_tokens().await.is_err() {
        panic!("Failed to load revoked access tokens");
    }

    Arc::new(AppState {
        jwt_manager,
        system,
    })
}

/// Starts the XITCA HTTP API server.
/// Returns the address the server is listening on.
pub async fn startxitca(config: HttpConfig, system: SharedSystem) -> (SocketAddr, ServerFuture) {
    let api_name = if config.tls.enabled {
        "XITCA HTTP API (TLS)"
    } else {
        "XITCA HTTP API"
    };

    let app_state: Arc<AppState> = build_app_state(&config, system).await;
    // start_expired_tokens_cleaner(app_state.clone());

    // construct http3 quic server config
    // let hconfig = h3_config(&config).unwrap();

    info!("Started {api_name} on: {:?}", config.address[1]);

    let listener = std::net::TcpListener::bind(config.address[1].clone()).unwrap();
    let address = listener
        .local_addr()
        .expect("Failed to get local address for HTTP server");

    let mut app = App::new();
    app = system::routes(app);
    app = users::routes(app);
    // app.with_state(app_state);

    let service = app
        .with_state(app_state)
        // .enclosed_fn(request_limit)
        .enclosed_fn(error_handler)
        .enclosed(SyncMiddleware::new(request_diagnostics))
        .enclosed(SyncMiddleware::new(metricsx))
        .enclosed(TowerHttpCompat::new(configure_cors(config.cors)))
        .enclosed_fn(middleware_fn)
        .enclosed(Logger::new())
        // limit client to 60rps based on it's ip address.
        .enclosed(RateLimit::per_minute(60))
        // middleware before App::finish have access to http request types.
        .finish()
        .enclosed(HttpServiceBuilder::new());
    // middleware after http service have access to raw connection types.
    // .enclosed_fn(connection_limit);

    let server = xitca_server::Builder::new()
        .bind("service_name", "127.0.0.1:8080", service)
        .unwrap()
        .build();
    // let server = app
    //     .with_state(app_state)
    //     .enclosed_fn(error_handler)
    //     .enclosed(SyncMiddleware::new(request_diagnostics))
    //     .enclosed(SyncMiddleware::new(metricsx))
    //     .enclosed(TowerHttpCompat::new(configure_cors(config.cors)))
    //     // .enclosed(TowerHttpCompat::new(
    //     //     tower::ServiceBuilder::new().layer(CorsLayer::very_permissive()),
    //     // ))
    //     .enclosed_fn(middleware_fn)
    //     .enclosed(Logger::new())
    //     .serve()
    //     .listen(listener)
    //     .unwrap()
    //     // .bind("127.0.0.1:8080")?
    //     .run();
    (address, server)
}

async fn handler_h1(
    _: Request<RequestExt<h1::RequestBody>>,
) -> Result<Response<ResponseBody>, Infallible> {
    Ok(Response::builder()
        .header(CONTENT_TYPE, TEXT_UTF8)
        .body("Hello World from Http/1!".into())
        .unwrap())
}
async fn handler_h3(
    _: Request<RequestExt<h3::RequestBody>>,
) -> Result<Response<ResponseBody>, Box<dyn std::error::Error>> {
    Response::builder()
        .status(200)
        .version(Version::HTTP_3)
        .header(CONTENT_TYPE, TEXT_UTF8)
        .body("Hello World from Http/3!".into())
        .map_err(Into::into)
}

fn generate_self_signed_cert(
) -> Result<(Vec<rustls::Certificate>, rustls::PrivateKey), Box<dyn Error>> {
    let certificate = rcgen::generate_simple_self_signed(vec!["localhost".into()]).unwrap();
    let certificate_der = certificate.serialize_der().unwrap();
    let private_key = certificate.serialize_private_key_der();
    let private_key = rustls::PrivateKey(private_key);
    let cert_chain = vec![rustls::Certificate(certificate_der)];
    Ok((cert_chain, private_key))
}
fn load_certificates(
    cert_file: &str,
    key_file: &str,
) -> Result<(Vec<rustls::Certificate>, rustls::PrivateKey), Box<dyn Error>> {
    let mut cert_chain_reader = BufReader::new(File::open(cert_file)?);
    let certs = rustls_pemfile::certs(&mut cert_chain_reader)
        .map(|x| rustls::Certificate(x.unwrap().to_vec()))
        .collect();
    let mut key_reader = BufReader::new(File::open(key_file)?);
    let mut keys = rustls_pemfile::rsa_private_keys(&mut key_reader)
        .map(|x| rustls::PrivateKey(x.unwrap().secret_pkcs1_der().to_vec()))
        .collect::<Vec<_>>();
    let key = rustls::PrivateKey(keys.remove(0).0);
    Ok((certs, key))
}
fn h3_config(config: &HttpConfig) -> io::Result<ServerConfig> {
    let (certificate, key) = match config.tls.enabled {
        true => generate_self_signed_cert().unwrap(),
        false => load_certificates(&config.tls.cert_file, &config.tls.key_file).unwrap(),
    };
    // let cert = fs::read("../../../certs/nigig_cert.pem")?;
    // let key = fs::read("../../../certs/nigig_key.pem")?;
    // let key = rustls_pemfile::pkcs8_private_keys(&mut &*key).remove(0);
    // let key = PrivateKey(key);
    // let cert = rustls_pemfile::certs(&mut &*cert)
    //     // .into_iter()
    //     .map(|res| res.unwrap())
    //     .collect();

    let mut acceptor = rustls::ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth()
        .with_single_cert(certificate, key)
        .unwrap();

    acceptor.alpn_protocols = vec![b"h3".to_vec()];

    Ok(ServerConfig::with_crypto(Arc::new(acceptor)))
}
