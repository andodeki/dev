// use crate::infrastructure::error::Error;
use crate::infrastructure::error::Error as InfraError;

use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum CustomError {
    #[error(transparent)]
    NigigServerError(#[from] InfraError),
}

#[derive(Debug, Serialize)]
pub struct ErrorResponse {
    pub id: u32,
    pub code: String,
    pub reason: String,
    pub field: Option<String>,
}

use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::Json;

impl IntoResponse for CustomError {
    fn into_response(self) -> Response {
        match self {
            CustomError::NigigServerError(error) => {
                let status_code = match error {
                    // Error::StreamIdNotFound(_) => StatusCode::NOT_FOUND,
                    // Error::TopicIdNotFound(_, _) => StatusCode::NOT_FOUND,
                    // Error::PartitionNotFound(_, _, _) => StatusCode::NOT_FOUND,
                    // Error::SegmentNotFound => StatusCode::NOT_FOUND,
                    // Error::ClientNotFound(_) => StatusCode::NOT_FOUND,
                    // Error::ConsumerGroupIdNotFound(_, _) => StatusCode::NOT_FOUND,
                    // Error::ConsumerGroupNameNotFound(_, _) => StatusCode::NOT_FOUND,
                    // Error::ConsumerGroupMemberNotFound(_, _, _) => StatusCode::NOT_FOUND,
                    // Error::CannotLoadResource(_) => StatusCode::NOT_FOUND,
                    // Error::ResourceNotFound(_) => StatusCode::NOT_FOUND,
                    // Error::IoError(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    // Error::WriteError(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    // Error::CannotParseInt(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    // Error::CannotParseSlice(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    // Error::CannotParseUtf8(_) => StatusCode::INTERNAL_SERVER_ERROR,
                    InfraError::Unauthenticated => StatusCode::UNAUTHORIZED,
                    InfraError::Unauthorized => StatusCode::FORBIDDEN,
                    _ => StatusCode::BAD_REQUEST,
                };
                (status_code, Json(ErrorResponse::from_error(error)))
            }
        }
        .into_response()
    }
}

impl ErrorResponse {
    pub fn from_error(error: InfraError) -> Self {
        ErrorResponse {
            id: error.as_code(),
            code: error.as_string().to_string(),
            reason: error.to_string(),
            field: match error {
                // Error::StreamIdNotFound(_) => Some("stream_id".to_string()),
                // Error::TopicIdNotFound(_, _) => Some("topic_id".to_string()),
                // Error::PartitionNotFound(_, _, _) => Some("partition_id".to_string()),
                // Error::SegmentNotFound => Some("segment_id".to_string()),
                // Error::ClientNotFound(_) => Some("client_id".to_string()),
                // Error::InvalidStreamName => Some("name".to_string()),
                // Error::StreamNameAlreadyExists(_) => Some("name".to_string()),
                // Error::InvalidTopicName => Some("name".to_string()),
                // Error::TopicNameAlreadyExists(_, _) => Some("name".to_string()),
                // Error::InvalidStreamId => Some("stream_id".to_string()),
                // Error::StreamIdAlreadyExists(_) => Some("stream_id".to_string()),
                // Error::InvalidTopicId => Some("topic_id".to_string()),
                // Error::TopicIdAlreadyExists(_, _) => Some("topic_id".to_string()),
                // Error::InvalidOffset(_) => Some("offset".to_string()),
                // Error::InvalidConsumerGroupId => Some("consumer_group_id".to_string()),
                // Error::ConsumerGroupIdAlreadyExists(_, _) => Some("consumer_group_id".to_string()),
                // Error::ConsumerGroupNameAlreadyExists(_, _) => Some("name".to_string()),
                InfraError::UserAlreadyExists => Some("username".to_string()),
                InfraError::PersonalAccessTokenAlreadyExists(_, _) => Some("name".to_string()),
                _ => None,
            },
        }
    }
}
use std::{convert::Infallible, error, fmt};
use xitca_web::{error::{Error, Internal}, http::WebResponse, service::Service, WebContext};

impl<'r, C> Service<WebContext<'r, C>> for CustomError {
    type Response = WebResponse;
    type Error = Infallible;

    async fn call(&self, ctx: WebContext<'r, C>) -> Result<Self::Response, Self::Error> {
        xitca_web::error::BadRequest.call(ctx).await
    }
}
impl<C> From<CustomError> for Error<C> {
    fn from(e: CustomError) -> Self {
        Error::from_service(e)
    }
}
pub async fn error_handler<S, C, Res>(s: &S, ctx: WebContext<'_, C>) -> Result<Res, Error<C>>
where
    S: for<'r> Service<WebContext<'r, C>, Response = Res, Error = Error<C>>,
{
    s.call(ctx).await.map_err(|e| {
        // debug format error info.
        println!("{e:?}");

        // display format error info.
        println!("{e}");

        // utilize std::error::Error trait methods for backtrace and more advanced error info.
        let _source = e.source();

        // // upcast trait and downcast to concrete type again.
        // // this offers the ability to regain typed error specific error handling.
        // // *. this is a runtime feature and not reinforced at compile time.
        // if let Some(e) = (&*e as &dyn error::Error).downcast_ref::<XitcaCustomError>() {
        //     match e {
        //         XitcaCustomError::NigigServerError => {}
        //     }
        // }

        e
    })
}

pub async fn catch_panic<S, C>(service: &S, ctx: WebContext<'_, C>) -> Result<WebResponse, Error<C>>
where
    S: for<'r> Service<WebContext<'r, C>, Response = WebResponse, Error = Error<C>>,
{
    use futures::FutureExt;
    std::panic::AssertUnwindSafe(service.call(ctx))
        .catch_unwind()
        .await
        // Internal is the default blank 500 error response type. we convert it to Error<C> type and
        // the default catch all convertor would help up construct a http response.
        .map_err(|_| Internal)?
}
// #[error_impl]
// impl CustomError {
//     async fn call<C>(&self, ctx: WebContext<'_, C>) -> WebResponse {
//         // logic of generating a response from your error.
//     }
// }
