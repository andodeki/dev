use crate::http::axum_http::jwt::jwt_manager::JwtManager;
use crate::infrastructure::systems::system::SharedSystem;
// use std::borrow::Borrow;
use std::net::SocketAddr;
use ulid::Ulid;
use xitca_codegen::State;

// #[derive(State, Clone, Debug, Eq, PartialEq)]
pub struct AppState {
    pub jwt_manager: JwtManager,
    pub system: SharedSystem,
}

// impl Borrow<JwtManager> for AppState {
//     fn borrow(&self) -> &JwtManager {
//         &self.jwt_manager
//     }
// }

// impl Borrow<SharedSystem> for AppState {
//     fn borrow(&self) -> &SharedSystem {
//         &self.system
//     }
// }

#[derive(State, Clone, Debug, Eq, PartialEq)]
pub struct DuplicateState {
    #[borrow]
    pub field1: String,
    #[borrow]
    pub field2: u32,
}

#[derive(Debug, Copy, Clone)]
pub struct RequestDetails {
    pub request_id: Ulid,
    pub ip_address: SocketAddr,
}
