use quinn::{ConnectionError, ReadToEndError, WriteError};
use std::array::TryFromSliceError;
use thiserror::Error;
use tokio::io;

// use xitca_web::{
//     bytes::Bytes,
//     http::{StatusCode, WebResponse}, WebContext,
// };

// use std::{convert::Infallible, error, fmt};

#[derive(Debug, Error)]
pub enum ServerError {
    #[error("IO error")]
    IoError(#[from] io::Error),
    #[error("System error")]
    SystemError(#[from] crate::infrastructure::error::Error),
    #[error("Connection error")]
    ConnectionError(#[from] ConnectionError),
    #[error("Invalid configuration provider: {0}")]
    InvalidConfigurationProvider(String),
    #[error("Cannot load configuration: {0}")]
    CannotLoadConfiguration(String),
    #[error("Invalid configuration")]
    InvalidConfiguration,
    #[error("SDK error")]
    SdkError(#[from] iggy::error::Error),
    #[error("Write error")]
    WriteError(#[from] WriteError),
    #[error("Read to end error")]
    ReadToEndError(#[from] ReadToEndError),
    #[error("Try from slice error")]
    TryFromSliceError(#[from] TryFromSliceError),
    #[error("Logging filter reload failure")]
    FilterReloadFailure,
    #[error("Logging stdout reload failure")]
    StdoutReloadFailure,
    #[error("Logging file reload failure")]
    FileReloadFailure,
    #[error("Cache config validation failure: {0}")]
    CacheConfigValidationFailure(String),
    #[error("error from /v2")]
    Index2,
    #[error("Command length error: {0}")]
    CommandLengthError(String),
}

// use xitca_web::{
//     bytes::Bytes,
//     // dev::service::Service,
//     // error::Error,
//     // handler::handler_service,
//     http::{StatusCode, WebResponse},
//     // route::get,
//     // App,
//     WebContext,
// };
// // error_impl is an attribute macro for http response generation
// #[xitca_web::codegen::error_impl]
// impl ServerError {
//     async fn call<C>(&self, ctx: WebContext<'_, C>) -> WebResponse {
//         let mut res = ctx.into_response(Bytes::new());
//         *res.status_mut() = StatusCode::BAD_REQUEST;
//         res
//     }
// }
