use crate::infrastructure::error::Error;
use crate::infrastructure::persistence::persister::Persister;
use crate::infrastructure::personal_access_tokens::personal_access_token::PersonalAccessToken;
use crate::infrastructure::personal_access_tokens::storage::FilePersonalAccessTokenStorage;
use crate::infrastructure::systems::info::SystemInfo;
use crate::infrastructure::systems::storage::FileSystemInfoStorage;
use crate::infrastructure::users::storage::FileUserStorage;
use crate::infrastructure::users::user::User;
use crate::models::user_info::UserId;
use async_trait::async_trait;
use sled::Db;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

#[async_trait]
pub trait Storage<T>: Sync + Send {
    async fn load(&self, component: &mut T) -> Result<(), Error>;
    async fn save(&self, component: &T) -> Result<(), Error>;
    async fn delete(&self, component: &T) -> Result<(), Error>;
}

#[async_trait]
pub trait SystemInfoStorage: Storage<SystemInfo> {}

#[async_trait]
pub trait UserStorage: Storage<User> {
    async fn load_by_id(&self, id: UserId) -> Result<User, Error>;
    async fn load_by_username(&self, username: &str) -> Result<User, Error>;
    async fn load_all(&self) -> Result<Vec<User>, Error>;
}

#[async_trait]
pub trait PersonalAccessTokenStorage: Storage<PersonalAccessToken> {
    async fn load_all(&self) -> Result<Vec<PersonalAccessToken>, Error>;
    async fn load_for_user(&self, user_id: UserId) -> Result<Vec<PersonalAccessToken>, Error>;
    async fn load_by_token(&self, token: &str) -> Result<PersonalAccessToken, Error>;
    async fn load_by_name(&self, user_id: UserId, name: &str)
        -> Result<PersonalAccessToken, Error>;
    async fn delete_for_user(&self, user_id: UserId, name: &str) -> Result<(), Error>;
}

#[derive(Debug)]
pub struct SystemStorage {
    pub info: Arc<dyn SystemInfoStorage>,
    pub user: Arc<dyn UserStorage>,
    pub personal_access_token: Arc<dyn PersonalAccessTokenStorage>,
}

impl SystemStorage {
    pub fn new(db: Arc<Db>) -> Self {
        Self {
            info: Arc::new(FileSystemInfoStorage::new(db.clone())),
            user: Arc::new(FileUserStorage::new(db.clone())),
            personal_access_token: Arc::new(FilePersonalAccessTokenStorage::new(db.clone())),
        }
    }
}

impl Debug for dyn SystemInfoStorage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "SystemInfoStorage")
    }
}

impl Debug for dyn UserStorage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "UserStorage")
    }
}

impl Debug for dyn PersonalAccessTokenStorage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "PersonalAccessTokenStorage")
    }
}

#[cfg(test)]
pub(crate) mod tests {
    use crate::infrastructure::{error::Error, systems::info::SystemInfo, users::user::User};
    use async_trait::async_trait;
    use std::sync::Arc;

    use super::*;

    struct TestSystemInfoStorage {}
    struct TestUserStorage {}
    struct TestPersonalAccessTokenStorage {}

    #[async_trait]
    impl Storage<SystemInfo> for TestSystemInfoStorage {
        async fn load(&self, _system_info: &mut SystemInfo) -> Result<(), Error> {
            Ok(())
        }

        async fn save(&self, _system_info: &SystemInfo) -> Result<(), Error> {
            Ok(())
        }

        async fn delete(&self, _system_info: &SystemInfo) -> Result<(), Error> {
            Ok(())
        }
    }

    #[async_trait]
    impl SystemInfoStorage for TestSystemInfoStorage {}

    #[async_trait]
    impl Storage<User> for TestUserStorage {
        async fn load(&self, _user: &mut User) -> Result<(), Error> {
            Ok(())
        }

        async fn save(&self, _user: &User) -> Result<(), Error> {
            Ok(())
        }

        async fn delete(&self, _user: &User) -> Result<(), Error> {
            Ok(())
        }
    }

    #[async_trait]
    impl UserStorage for TestUserStorage {
        async fn load_by_id(&self, _id: UserId) -> Result<User, Error> {
            Ok(User::default())
        }

        async fn load_by_username(&self, _username: &str) -> Result<User, Error> {
            Ok(User::default())
        }

        async fn load_all(&self) -> Result<Vec<User>, Error> {
            Ok(vec![])
        }
    }

    #[async_trait]
    impl Storage<PersonalAccessToken> for TestPersonalAccessTokenStorage {
        async fn load(
            &self,
            _personal_access_token: &mut PersonalAccessToken,
        ) -> Result<(), Error> {
            Ok(())
        }

        async fn save(&self, _personal_access_token: &PersonalAccessToken) -> Result<(), Error> {
            Ok(())
        }

        async fn delete(&self, _personal_access_token: &PersonalAccessToken) -> Result<(), Error> {
            Ok(())
        }
    }

    #[async_trait]
    impl PersonalAccessTokenStorage for TestPersonalAccessTokenStorage {
        async fn load_all(&self) -> Result<Vec<PersonalAccessToken>, Error> {
            Ok(vec![])
        }

        async fn load_for_user(&self, _user_id: UserId) -> Result<Vec<PersonalAccessToken>, Error> {
            Ok(vec![])
        }

        async fn load_by_token(&self, _token: &str) -> Result<PersonalAccessToken, Error> {
            Ok(PersonalAccessToken::default())
        }

        async fn load_by_name(
            &self,
            _user_id: UserId,
            _name: &str,
        ) -> Result<PersonalAccessToken, Error> {
            Ok(PersonalAccessToken::default())
        }

        async fn delete_for_user(&self, _user_id: UserId, _name: &str) -> Result<(), Error> {
            Ok(())
        }
    }

    pub fn get_test_system_storage() -> SystemStorage {
        SystemStorage {
            info: Arc::new(TestSystemInfoStorage {}),
            user: Arc::new(TestUserStorage {}),
            personal_access_token: Arc::new(TestPersonalAccessTokenStorage {}),
        }
    }
}
