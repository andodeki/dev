use prometheus_client::encoding::text::encode;
use prometheus_client::metrics::counter::Counter;
use prometheus_client::metrics::gauge::Gauge;
use prometheus_client::registry::Registry;
use tracing::error;

#[derive(Debug)]
pub(crate) struct Metrics {
    registry: Registry,
    http_requests: Counter,
    users: Gauge,
    clients: Gauge,
}

impl Metrics {
    pub fn init() -> Self {
        let mut metrics = Metrics {
            registry: <Registry>::default(),
            http_requests: Counter::default(),
            users: Gauge::default(),
            clients: Gauge::default(),
        };

        metrics.register_counter("http_requests", metrics.http_requests.clone());
        metrics.register_gauge("users", metrics.users.clone());
        metrics.register_gauge("clients", metrics.clients.clone());

        metrics
    }

    fn register_counter(&mut self, name: &str, counter: Counter) {
        self.registry
            .register(name, format!("total count of {name}"), counter)
    }

    fn register_gauge(&mut self, name: &str, gauge: Gauge) {
        self.registry
            .register(name, format!("total count of {name}"), gauge)
    }

    pub fn get_formatted_output(&self) -> String {
        let mut buffer = String::new();
        if let Err(err) = encode(&mut buffer, &self.registry) {
            error!("Failed to encode metrics: {}", err);
        }
        buffer
    }

    pub fn increment_http_requests(&self) {
        self.http_requests.inc();
    }

    pub fn increment_users(&self, count: u32) {
        self.users.inc_by(count as i64);
    }

    pub fn decrement_users(&self, count: u32) {
        self.users.dec_by(count as i64);
    }

    pub fn increment_clients(&self, count: u32) {
        self.clients.inc_by(count as i64);
    }

    pub fn decrement_clients(&self, count: u32) {
        self.clients.dec_by(count as i64);
    }
}
