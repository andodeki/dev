// use crate::binary::handlers::consumer_groups::{
//     create_consumer_group_handler, delete_consumer_group_handler, get_consumer_group_handler,
//     get_consumer_groups_handler, join_consumer_group_handler, leave_consumer_group_handler,
// };
// use crate::binary::handlers::consumer_offsets::*;
// use crate::binary::handlers::messages::*;
// use crate::binary::handlers::partitions::*;
use crate::binary::handlers::personal_access_tokens::{
    create_personal_access_token_handler, delete_personal_access_token_handler,
    get_personal_access_tokens_handler, login_with_personal_access_token_handler,
};
// use crate::binary::handlers::streams::*;
use crate::binary::handlers::system::*;
// use crate::binary::handlers::topics::*;
use crate::binary::handlers::users::{
    change_password_handler, create_user_handler, delete_user_handler, get_user_handler,
    get_users_handler, login_user_handler, logout_user_handler, update_permissions_handler,
    update_user_handler,
};
use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::command::Command;
use tracing::debug;

pub async fn handle(
    command: &Command,
    sender: &mut dyn Sender,
    session: &Session,
    system: SharedSystem,
) -> Result<(), Error> {
    let result = try_handle(command, sender, session, &system).await;
    if result.is_ok() {
        debug!("Command was handled successfully, session: {session}.",);
        return Ok(());
    }

    let error = result.err().unwrap();
    debug!("Command was not handled successfully, session: {session}, error: {error}.",);
    sender.send_error_response(error).await?;
    Ok(())
}

async fn try_handle(
    command: &Command,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("Handling command '{command}', session: {session}...");
    match command {
        Command::Ping(command) => ping_handler::handle(command, sender, session).await,
        Command::GetStats(command) => {
            get_stats_handler::handle(command, sender, session, system).await
        }
        Command::GetMe(command) => get_me_handler::handle(command, sender, session, system).await,
        Command::GetClient(command) => {
            get_client_handler::handle(command, sender, session, system).await
        }
        Command::GetClients(command) => {
            get_clients_handler::handle(command, sender, session, system).await
        }
        Command::GetUser(command) => {
            get_user_handler::handle(command, sender, session, system).await
        }
        Command::GetUsers(command) => {
            get_users_handler::handle(command, sender, session, system).await
        }
        Command::CreateUser(command) => {
            create_user_handler::handle(command, sender, session, system).await
        }
        Command::DeleteUser(command) => {
            delete_user_handler::handle(command, sender, session, system).await
        }
        Command::UpdateUser(command) => {
            update_user_handler::handle(command, sender, session, system).await
        }
        Command::UpdatePermissions(command) => {
            update_permissions_handler::handle(command, sender, session, system).await
        }
        Command::ChangePassword(command) => {
            change_password_handler::handle(command, sender, session, system).await
        }
        Command::LoginUser(command) => {
            login_user_handler::handle(command, sender, session, system).await
        }
        Command::LogoutUser(command) => {
            logout_user_handler::handle(command, sender, session, system).await
        }
        Command::GetPersonalAccessTokens(command) => {
            get_personal_access_tokens_handler::handle(command, sender, session, system).await
        }
        Command::CreatePersonalAccessToken(command) => {
            create_personal_access_token_handler::handle(command, sender, session, system).await
        }
        Command::DeletePersonalAccessToken(command) => {
            delete_personal_access_token_handler::handle(command, sender, session, system).await
        }
        Command::LoginWithPersonalAccessToken(command) => {
            login_with_personal_access_token_handler::handle(command, sender, session, system).await
        }
    }
}
