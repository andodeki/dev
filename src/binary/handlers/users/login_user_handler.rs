use crate::binary::mapper;
use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::users::login_user::LoginUser;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &LoginUser,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    let user = system
        .login_user(&command.username, &command.password, Some(session))
        .await?;
    let identity_info = mapper::map_identity_info(user.id);
    sender.send_ok_response(identity_info.as_slice()).await?;
    Ok(())
}
