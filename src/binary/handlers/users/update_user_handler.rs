use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::users::update_user::UpdateUser;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &UpdateUser,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    system
        .update_user(
            session,
            &command.user_id,
            command.username.clone(),
            command.status,
        )
        .await?;
    sender.send_empty_ok_response().await?;
    Ok(())
}
