use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::users::logout_user::LogoutUser;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &LogoutUser,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    system.logout_user(session).await?;
    session.clear_user_id();
    sender.send_empty_ok_response().await?;
    Ok(())
}
