use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::users::update_permissions::UpdatePermissions;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &UpdatePermissions,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let mut system = system.write();
    system
        .update_permissions(session, &command.user_id, command.permissions.clone())
        .await?;
    sender.send_empty_ok_response().await?;
    Ok(())
}
