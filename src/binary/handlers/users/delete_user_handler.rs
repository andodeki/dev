use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::users::delete_user::DeleteUser;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &DeleteUser,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let mut system = system.write();
    system.delete_user(session, &command.user_id).await?;
    sender.send_empty_ok_response().await?;
    Ok(())
}
