use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::models::system::ping::Ping;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &Ping,
    sender: &mut dyn Sender,
    session: &Session,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    sender.send_empty_ok_response().await?;
    Ok(())
}
