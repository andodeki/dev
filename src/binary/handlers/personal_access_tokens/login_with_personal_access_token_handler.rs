use crate::binary::mapper;
use crate::binary::sender::Sender;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::personal_access_tokens::login_with_personal_access_token::LoginWithPersonalAccessToken;
// use crate::models::personal_access_tokens::login_with_personal_access_token::LoginWithPersonalAccessToken;
use crate::infrastructure::error::Error;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &LoginWithPersonalAccessToken,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    let user = system
        .login_with_personal_access_token(&command.token, Some(session))
        .await?;
    let identity_info = mapper::map_identity_info(user.id);
    sender.send_ok_response(identity_info.as_slice()).await?;
    Ok(())
}
