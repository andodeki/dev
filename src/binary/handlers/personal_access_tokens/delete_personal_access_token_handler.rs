use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::personal_access_tokens::delete_personal_access_token::DeletePersonalAccessToken;
// use crate::models::personal_access_tokens::create_personal_access_token::DeletePersonalAccessToken;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &DeletePersonalAccessToken,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    system
        .delete_personal_access_token(session, &command.name)
        .await?;
    sender.send_empty_ok_response().await?;
    Ok(())
}
