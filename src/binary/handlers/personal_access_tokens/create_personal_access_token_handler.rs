use crate::binary::mapper;
use crate::binary::sender::Sender;
use crate::infrastructure::error::Error;
use crate::infrastructure::session::Session;
use crate::infrastructure::systems::system::SharedSystem;
use crate::models::personal_access_tokens::create_personal_access_token::CreatePersonalAccessToken;
use anyhow::Result;
use tracing::debug;

pub async fn handle(
    command: &CreatePersonalAccessToken,
    sender: &mut dyn Sender,
    session: &Session,
    system: &SharedSystem,
) -> Result<(), Error> {
    debug!("session: {session}, command: {command}");
    let system = system.read();
    let token = system
        .create_personal_access_token(session, &command.name, command.expiry)
        .await?;
    let bytes = mapper::map_raw_pat(&token);
    sender.send_ok_response(bytes.as_slice()).await?;
    Ok(())
}
