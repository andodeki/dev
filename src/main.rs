#![allow(warnings)]
#![allow(stable_features)]

use clap::Parser;
use dev::channels::commands::clean_personal_access_tokens::CleanPersonalAccessTokensExecutor;
use dev::channels::handler::ServerCommandHandler;
use dev::quic::quic_server;
use dev::tcp::tcp_server;
use figlet_rs::FIGfont;
// use std::sync::Arc;
use tokio::time::Instant;
use tracing::info;

use dev::infrastructure::systems::system::{SharedSystem, System};
use dev::{
    args::Args,
    configs::{config_provider, server::ServerConfig},
    logging::Logging,
    server_error::ServerError,
    // tcp::tcp_server::start,
};
// use dev::http::http_server;
use dev::configs::http::HttpConfig;

#[derive(Debug, Parser)]
struct Cli {
    /// The port to run the server on
    port: i32,
}
// cfg_if! {
//     if #[cfg(feature = "axum")] {
#[tokio::main]
async fn main() -> Result<(), ServerError> {
    // async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let startup_timestamp = Instant::now();
    let standard_font = FIGfont::standard().unwrap();
    let figure = standard_font.convert("App Server");
    println!("{}", figure.unwrap());

    let mut logging = Logging::new();
    logging.early_init();

    // env_logger::init();
    // let args = Cli::parse();
    let args = Args::parse();

    info!("Parsed arguments: {:?}", args);

    let config_provider = config_provider::resolve(&args.config_provider)?;
    let config = ServerConfig::load(config_provider.as_ref()).await?;

    logging.late_init(config.system.get_system_path(), &config.system.logging)?;

    // let mut system = System::new(config.system.clone(), None, config.personal_access_token);
    let mut system = System::new(config.system.clone(), None, config.personal_access_token);

    system.init().await?;
    let system = SharedSystem::new(system);
    let _command_handler = ServerCommandHandler::new(system.clone(), &config)
        // .install_handler(SaveMessagesExecutor)
        // .install_handler(CleanMessagesExecutor)
        .install_handler(CleanPersonalAccessTokensExecutor);

    #[cfg(unix)]
    let (mut ctrl_c, mut sigterm) = {
        use tokio::signal::unix::{signal, SignalKind};
        (
            signal(SignalKind::interrupt())?,
            signal(SignalKind::terminate())?,
        )
    };
    let mut current_config = config.clone();

    if config.quic.enabled {
        let quic_addr = quic_server::start(config.quic, system.clone());
        current_config.quic.address = quic_addr.to_string();
    }
    // if config.tcp.enabled {
    //     // run(args.port).await;
    //     // dev::tcp::tcp_server::start(config.tcp);
    //     // let http_addr = dev::tcp::tcp_server::start(config.http, system.clone()).await;
    //     let _tcp_addr = dev::tcp::tcp_server::start(config.tcp, system.clone()).await;
    //     // current_config.tcp.address = tcp_addr.to_string();
    //     // tcp_server::start(config.tcp, system.clone());
    // }
    if config.tcp.enabled {
        let tcp_addr = tcp_server::start(config.tcp, system.clone()).await;
        current_config.tcp.address = tcp_addr.to_string();
    }
    if config.http.variants.axum_enabled {
        let system = system.clone();
        // tokio::spawn(async move {
        // dev::http::http_server::start(config.http, system).await;
        // dev::http::testserver::start(config.http, system).await;
        //
        let http_addr =
            dev::http::axum_http::http_server::start(&config.http, system.clone()).await;
        current_config.http.address[0] = http_addr.to_string();
        // });
    }
    if config.http.variants.xitca_enabled {
        let system = system.clone();
        let (http_addr, srv) =
            dev::http::xitcav_http::http_server::startxitca(config.http, system.clone()).await;
        srv.await;
        current_config.http.address[1] = http_addr.to_string();
    }

    let runtime_path = current_config.system.get_runtime_path();
    // tracing::warn!("================\n{}", runtime_path);
    let current_config_path = format!("{}/current_config.toml", runtime_path);
    let current_config_content =
        toml::to_string(&current_config).expect("Cannot serialize current_config");

    // tracing::warn!("===============\n{}", current_config_content);
    // tracing::warn!("===============\n{}", current_config_path);
    tokio::fs::write(current_config_path, current_config_content).await?;

    let elapsed_time = startup_timestamp.elapsed();
    info!(
        "dev server has started - overall startup took {} ms.",
        elapsed_time.as_millis()
    );
    #[cfg(unix)]
    tokio::select! {
        _ = ctrl_c.recv() => {
            info!("Received SIGINT. Shutting down dev server...");
        },
        _ = sigterm.recv() => {
            info!("Received SIGTERM. Shutting down dev server...");
        }
    }

    let shutdown_timestamp = Instant::now();
    // let mut system = system.write().await;
    // let persister = Arc::new(FileWithSyncPersister);
    // let storage = Arc::new(FileSegmentStorage::new(persister));
    // system.shutdown(storage).await?;
    let elapsed_time = shutdown_timestamp.elapsed();

    info!(
        "dev server has shutdown successfully. Shutdown took {} ms.",
        elapsed_time.as_millis()
    );
    Ok(())
}

//     } else if #[cfg(feature = "xitca")] {
//         use xitca_codegen::State;
//         use xitca_http::Request;
//         use xitca_unsafe_collection::futures::NowOrPanic;

//         use xitca_web::{
//             App,WebContext,
//             body::RequestBody,
//             dev::service::Service,
//             handler::{
//                 extension::ExtensionRef, extension::ExtensionsRef, handler_service, path::PathRef, state::StateRef,
//                 uri::UriRef,
//             },
//             http::{const_header_value::TEXT_UTF8, header::CONTENT_TYPE, Method, Uri},
//             middleware::UncheckedReady,
//             route::get,
//         };
//         use std::net::SocketAddr;

//         #[derive(State, Clone, Debug, Eq, PartialEq)]
//         struct State {
//             #[borrow]
//             field1: String,
//             #[borrow]
//             field2: u32,
//         }

//         async fn handler(
//             StateRef(state): StateRef<'_, String>,
//             StateRef(state2): StateRef<'_, u32>,
//             StateRef(state3): StateRef<'_, State>,
//             ctx: &WebContext<'_, State>,
//         ) -> String {
//             assert_eq!("state", state);
//             assert_eq!(&996, state2);
//             assert_eq!(state, ctx.state().field1.as_str());
//             assert_eq!(state3, ctx.state());
//             state.to_string()
//         }
//         // fn main() -> std::io::Result<()> {
//         fn main() -> Result<(), ServerError> {
//             tracing_subscriber::fmt()
//                 .with_env_filter("xitca=info,[xitca-logger]=trace")
//                 .init();

//             let startup_timestamp = Instant::now();
//             let standard_font = FIGfont::standard().unwrap();
//             let figure = standard_font.convert("App Server");
//             println!("{}", figure.unwrap());

//             let mut logging = Logging::new();
//             logging.early_init();

//             // env_logger::init();
//             // let args = Cli::parse();
//             let args = Args::parse();

//             info!("Parsed arguments: {:?}", args);

//             // let config_provider = config_provider::resolve(&args.config_provider).map_err(|err| {
//             //     // Your code when config_provider::resolve fails
//             //     tracing::error!("Error resolving config provider: {}", err);
//             //     // Handle the error appropriately, e.g., return an Err or panic
//             // });;
//             // let config = ServerConfig::load(config_provider.as_ref());

//             // logging.late_init(config.system.get_system_path(), &config.system.logging).map_err(|err| {
//             //     // Your code when config_provider::resolve fails
//             //     tracing::error!("Error resolving config provider: {}", err);
//             //     // Handle the error appropriately, e.g., return an Err or panic
//             // });

//             // // let mut system = System::new(config.system.clone(), None, config.personal_access_token);
//             // let mut system = System::new(config.system.clone(), None, config.personal_access_token);

//             // system.init().await.map_err(|err| {
//             //     // Your code when config_provider::resolve fails
//             //     tracing::error!("Error resolving config provider: {}", err);
//             //     // Handle the error appropriately, e.g., return an Err or panic
//             // });
//             // let system = SharedSystem::new(system);

//             // if config.http.enabled {
//             //     let system = system.clone();
//             //     tokio::spawn(async move {
//             //         startxitca(config.http, system).await;
//             //     });
//             // }
//             Ok(())

//             // .finish()
//             // .call(())
//             // .now_or_panic()
//             // .ok()
//             // .unwrap()
//             // .call(Request::default())
//             // .now_or_panic()
//             // .unwrap()
//     }
//         pub async fn startxitca(config: HttpConfig, system: SharedSystem) -> SocketAddr {
//             let api_name = if config.tls.enabled {
//                 "HTTP API (TLS)"
//             } else {
//                 "HTTP API"
//             };

//             let listener = tokio::net::TcpListener::bind(config.address).await.unwrap();
//             let address = listener
//                 .local_addr()
//                 .expect("Failed to get local address for HTTP server");

//             let state = State {
//                 field1: String::from("state"),
//                 field2: 996,
//             };

//             tokio::task::spawn(async move {
//                 // axum::serve(
//                 //     listener,
//                 //     app.into_make_service_with_connect_info::<SocketAddr>(),
//                 // )
//                 // .await
//                 // .expect("Failed to start HTTP server");
//                 App::with_state(state)
//                     .at("/", get(handler_service(handler)))
//                     .serve()
//                     .bind("127.0.0.1:8080").expect("REASON")
//                     .run()
//                     .wait();
//             });

//             address
//         }

//     } else {
//         pub fn main() {

//         }
//     }
// }
