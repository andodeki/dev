use crate::infrastructure::error::Error;
use crate::models::binary::binary_client::BinaryClient;
use crate::models::binary::{fail_if_not_authenticated, mapper};
use crate::models::bytes_serializable::BytesSerializable;
use crate::models::client::SystemClient;
use crate::models::client_info::{ClientInfo, ClientInfoDetails};
use crate::models::command::{
    GET_CLIENTS_CODE, GET_CLIENT_CODE, GET_ME_CODE, GET_STATS_CODE, PING_CODE,
};
use crate::models::stats::Stats;
use crate::models::system::get_client::GetClient;
use crate::models::system::get_clients::GetClients;
use crate::models::system::get_me::GetMe;
use crate::models::system::get_stats::GetStats;
use crate::models::system::ping::Ping;

#[async_trait::async_trait]
impl<B: BinaryClient> SystemClient for B {
    async fn get_stats(&self, command: &GetStats) -> Result<Stats, Error> {
        fail_if_not_authenticated(self).await?;
        let response = self
            .send_with_response(GET_STATS_CODE, command.as_bytes())
            .await?;
        mapper::map_stats(response)
    }

    async fn get_me(&self, command: &GetMe) -> Result<ClientInfoDetails, Error> {
        fail_if_not_authenticated(self).await?;
        let response = self
            .send_with_response(GET_ME_CODE, command.as_bytes())
            .await?;
        mapper::map_client(response)
    }

    async fn get_client(&self, command: &GetClient) -> Result<ClientInfoDetails, Error> {
        fail_if_not_authenticated(self).await?;
        let response = self
            .send_with_response(GET_CLIENT_CODE, command.as_bytes())
            .await?;
        mapper::map_client(response)
    }

    async fn get_clients(&self, command: &GetClients) -> Result<Vec<ClientInfo>, Error> {
        fail_if_not_authenticated(self).await?;
        let response = self
            .send_with_response(GET_CLIENTS_CODE, command.as_bytes())
            .await?;
        mapper::map_clients(response)
    }

    async fn ping(&self, command: &Ping) -> Result<(), Error> {
        self.send_with_response(PING_CODE, command.as_bytes())
            .await?;
        Ok(())
    }
}
