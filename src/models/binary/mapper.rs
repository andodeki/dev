use crate::infrastructure::error::Error;
use crate::models::bytes_serializable::BytesSerializable;
use crate::models::client_info::{ClientInfo, ClientInfoDetails};
// use crate::models::consumer_group::{ConsumerGroup, ConsumerGroupDetails, ConsumerGroupMember};
// use crate::models::consumer_offset_info::ConsumerOffsetInfo;
use crate::models::identity_info::IdentityInfo;
// use crate::models::messages::{Message, MessageState, PolledMessages};
// use crate::models::partition::Partition;
use crate::models::permissions::Permissions;
use crate::models::personal_access_token::{PersonalAccessTokenInfo, RawPersonalAccessToken};
use crate::models::stats::Stats;
// use crate::models::stream::{Stream, StreamDetails};
// use crate::models::topic::{Topic, TopicDetails};
use crate::models::user_info::{UserInfo, UserInfoDetails};
use crate::models::user_status::UserStatus;
// use bytes::Bytes;
// use std::collections::HashMap;
use bytes::Bytes;
use std::str::from_utf8;

// const EMPTY_MESSAGES: Vec<Message> = vec![];
// const EMPTY_TOPICS: Vec<Topic> = vec![];
// const EMPTY_STREAMS: Vec<Stream> = vec![];
const EMPTY_CLIENTS: Vec<ClientInfo> = vec![];
const EMPTY_USERS: Vec<UserInfo> = vec![];
const EMPTY_PERSONAL_ACCESS_TOKENS: Vec<PersonalAccessTokenInfo> = vec![];
// const EMPTY_CONSUMER_GROUPS: Vec<ConsumerGroup> = vec![];

pub fn map_stats(payload: Bytes) -> Result<Stats, Error> {
    let process_id = u32::from_le_bytes(payload[..4].try_into()?);
    let cpu_usage = f32::from_le_bytes(payload[4..8].try_into()?);
    let memory_usage = u64::from_le_bytes(payload[8..16].try_into()?).into();
    let total_memory = u64::from_le_bytes(payload[16..24].try_into()?).into();
    let available_memory = u64::from_le_bytes(payload[24..32].try_into()?).into();
    let run_time = u64::from_le_bytes(payload[32..40].try_into()?);
    let start_time = u64::from_le_bytes(payload[40..48].try_into()?);
    let read_bytes = u64::from_le_bytes(payload[48..56].try_into()?).into();
    let written_bytes = u64::from_le_bytes(payload[56..64].try_into()?).into();
    // let total_size_bytes = u64::from_le_bytes(payload[64..72].try_into()?).into();
    // let streams_count = u32::from_le_bytes(payload[72..76].try_into()?);
    // let topics_count = u32::from_le_bytes(payload[76..80].try_into()?);
    // let partitions_count = u32::from_le_bytes(payload[80..84].try_into()?);
    // let segments_count = u32::from_le_bytes(payload[84..88].try_into()?);
    // let messages_count = u64::from_le_bytes(payload[88..96].try_into()?);
    let clients_count = u32::from_le_bytes(payload[96..100].try_into()?);
    // let consumer_groups_count = u32::from_le_bytes(payload[100..104].try_into()?);
    let mut current_position = 104;
    let hostname_length =
        u32::from_le_bytes(payload[current_position..current_position + 4].try_into()?) as usize;
    let hostname =
        from_utf8(&payload[current_position + 4..current_position + 4 + hostname_length])?
            .to_string();
    current_position += 4 + hostname_length;
    let os_name_length =
        u32::from_le_bytes(payload[current_position..current_position + 4].try_into()?) as usize;
    let os_name = from_utf8(&payload[current_position + 4..current_position + 4 + os_name_length])?
        .to_string();
    current_position += 4 + os_name_length;
    let os_version_length =
        u32::from_le_bytes(payload[current_position..current_position + 4].try_into()?) as usize;
    let os_version =
        from_utf8(&payload[current_position + 4..current_position + 4 + os_version_length])?
            .to_string();
    current_position += 4 + os_version_length;
    let kernel_version_length =
        u32::from_le_bytes(payload[current_position..current_position + 4].try_into()?) as usize;
    let kernel_version =
        from_utf8(&payload[current_position + 4..current_position + 4 + kernel_version_length])?
            .to_string();

    Ok(Stats {
        process_id,
        cpu_usage,
        memory_usage,
        total_memory,
        available_memory,
        run_time,
        start_time,
        read_bytes,
        written_bytes,
        // messages_size_bytes: total_size_bytes,
        // streams_count,
        // topics_count,
        // partitions_count,
        // segments_count,
        // messages_count,
        clients_count,
        // consumer_groups_count,
        hostname,
        os_name,
        os_version,
        kernel_version,
    })
}

pub fn map_user(payload: Bytes) -> Result<UserInfoDetails, Error> {
    let (user, position) = map_to_user_info(payload.clone(), 0)?;
    let has_permissions = payload[position];
    let permissions = if has_permissions == 1 {
        let permissions_length =
            u32::from_le_bytes(payload[position + 1..position + 5].try_into()?) as usize;
        let permissions = payload.slice(position + 5..position + 5 + permissions_length);
        Some(Permissions::from_bytes(permissions)?)
    } else {
        None
    };

    let user = UserInfoDetails {
        id: user.id,
        created_at: user.created_at,
        status: user.status,
        username: user.username,
        permissions,
    };
    Ok(user)
}

pub fn map_users(payload: Bytes) -> Result<Vec<UserInfo>, Error> {
    if payload.is_empty() {
        return Ok(EMPTY_USERS);
    }

    let mut users = Vec::new();
    let length = payload.len();
    let mut position = 0;
    while position < length {
        let (user, read_bytes) = map_to_user_info(payload.clone(), position)?;
        users.push(user);
        position += read_bytes;
    }
    users.sort_by(|x, y| x.id.cmp(&y.id));
    Ok(users)
}

pub fn map_personal_access_tokens(payload: Bytes) -> Result<Vec<PersonalAccessTokenInfo>, Error> {
    if payload.is_empty() {
        return Ok(EMPTY_PERSONAL_ACCESS_TOKENS);
    }

    let mut personal_access_tokens = Vec::new();
    let length = payload.len();
    let mut position = 0;
    while position < length {
        let (personal_access_token, read_bytes) = map_to_pat_info(payload.clone(), position)?;
        personal_access_tokens.push(personal_access_token);
        position += read_bytes;
    }
    personal_access_tokens.sort_by(|x, y| x.name.cmp(&y.name));
    Ok(personal_access_tokens)
}

pub fn map_identity_info(payload: Bytes) -> Result<IdentityInfo, Error> {
    let user_id = u32::from_le_bytes(payload[..4].try_into()?);
    Ok(IdentityInfo {
        user_id,
        tokens: None,
    })
}

pub fn map_raw_pat(payload: Bytes) -> Result<RawPersonalAccessToken, Error> {
    let token_length = payload[0];
    let token = from_utf8(&payload[1..1 + token_length as usize])?.to_string();
    Ok(RawPersonalAccessToken { token })
}

pub fn map_client(payload: Bytes) -> Result<ClientInfoDetails, Error> {
    let (client, mut position) = map_to_client_info(payload.clone(), 0)?;
    // let mut consumer_groups = Vec::new();
    let length = payload.len();
    // while position < length {
    // for _ in 0..client.consumer_groups_count {
    //     let stream_id = u32::from_le_bytes(payload[position..position + 4].try_into()?);
    //     let topic_id = u32::from_le_bytes(payload[position + 4..position + 8].try_into()?);
    //     let consumer_group_id =
    //         u32::from_le_bytes(payload[position + 8..position + 12].try_into()?);
    //     let consumer_group = ConsumerGroupInfo {
    //         stream_id,
    //         topic_id,
    //         consumer_group_id,
    //     };
    //     consumer_groups.push(consumer_group);
    //     position += 12;
    // }
    // }

    // consumer_groups.sort_by(|x, y| x.consumer_group_id.cmp(&y.consumer_group_id));
    let client = ClientInfoDetails {
        client_id: client.client_id,
        user_id: client.user_id,
        address: client.address,
        transport: client.transport,
        // consumer_groups_count: client.consumer_groups_count,
        // consumer_groups,
    };
    Ok(client)
}

pub fn map_clients(payload: Bytes) -> Result<Vec<ClientInfo>, Error> {
    if payload.is_empty() {
        return Ok(EMPTY_CLIENTS);
    }

    let mut clients = Vec::new();
    let length = payload.len();
    let mut position = 0;
    while position < length {
        let (client, read_bytes) = map_to_client_info(payload.clone(), position)?;
        clients.push(client);
        position += read_bytes;
    }
    clients.sort_by(|x, y| x.client_id.cmp(&y.client_id));
    Ok(clients)
}

fn map_to_client_info(payload: Bytes, mut position: usize) -> Result<(ClientInfo, usize), Error> {
    let mut read_bytes;
    let client_id = u32::from_le_bytes(payload[position..position + 4].try_into()?);
    let user_id = u32::from_le_bytes(payload[position + 4..position + 8].try_into()?);
    let user_id = match user_id {
        0 => None,
        _ => Some(user_id),
    };

    let transport = payload[position + 8];
    let transport = match transport {
        1 => "TCP",
        2 => "QUIC",
        _ => "Unknown",
    }
    .to_string();

    let address_length =
        u32::from_le_bytes(payload[position + 9..position + 13].try_into()?) as usize;
    let address = from_utf8(&payload[position + 13..position + 13 + address_length])?.to_string();
    read_bytes = 4 + 4 + 1 + 4 + address_length;
    position += read_bytes;
    let consumer_groups_count = u32::from_le_bytes(payload[position..position + 4].try_into()?);
    read_bytes += 4;
    Ok((
        ClientInfo {
            client_id,
            user_id,
            address,
            transport,
            consumer_groups_count,
        },
        read_bytes,
    ))
}

fn map_to_user_info(payload: Bytes, position: usize) -> Result<(UserInfo, usize), Error> {
    let id = u32::from_le_bytes(payload[position..position + 4].try_into()?);
    let created_at = u64::from_le_bytes(payload[position + 4..position + 12].try_into()?);
    let status = payload[position + 12];
    let status = UserStatus::from_code(status)?;
    let username_length = payload[position + 13];
    let username =
        from_utf8(&payload[position + 14..position + 14 + username_length as usize])?.to_string();
    let read_bytes = 4 + 8 + 1 + 1 + username_length as usize;

    Ok((
        UserInfo {
            id,
            created_at,
            status,
            username,
        },
        read_bytes,
    ))
}

fn map_to_pat_info(
    payload: Bytes,
    position: usize,
) -> Result<(PersonalAccessTokenInfo, usize), Error> {
    let name_length = payload[position];
    let name = from_utf8(&payload[position + 1..position + 1 + name_length as usize])?.to_string();
    let position = position + 1 + name_length as usize;
    let expiry = u64::from_le_bytes(payload[position..position + 8].try_into()?);
    let expiry = match expiry {
        0 => None,
        _ => Some(expiry),
    };
    let read_bytes = 1 + name_length as usize + 8;

    Ok((PersonalAccessTokenInfo { name, expiry }, read_bytes))
}
