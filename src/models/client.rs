use crate::infrastructure::error::Error;
use crate::models::client_info::{ClientInfo, ClientInfoDetails};
use crate::models::identity_info::IdentityInfo;
use crate::models::personal_access_token::{PersonalAccessTokenInfo, RawPersonalAccessToken};
use crate::models::personal_access_tokens::create_personal_access_token::CreatePersonalAccessToken;
use crate::models::personal_access_tokens::delete_personal_access_token::DeletePersonalAccessToken;
use crate::models::personal_access_tokens::get_personal_access_tokens::GetPersonalAccessTokens;
use crate::models::personal_access_tokens::login_with_personal_access_token::LoginWithPersonalAccessToken;
use crate::models::stats::Stats;
use crate::models::system::get_client::GetClient;
use crate::models::system::get_clients::GetClients;
use crate::models::system::get_me::GetMe;
use crate::models::system::get_stats::GetStats;
use crate::models::system::ping::Ping;
use crate::models::user_info::{UserInfo, UserInfoDetails};
use crate::models::users::change_password::ChangePassword;
use crate::models::users::create_user::CreateUser;
use crate::models::users::delete_user::DeleteUser;
use crate::models::users::get_user::GetUser;
use crate::models::users::get_users::GetUsers;
use crate::models::users::login_user::LoginUser;
use crate::models::users::logout_user::LogoutUser;
use crate::models::users::update_permissions::UpdatePermissions;
use crate::models::users::update_user::UpdateUser;
use async_trait::async_trait;
use std::fmt::Debug;

/// The client is the main interface to the Iggy server.
/// It consists of multiple modules, each of which is responsible for a specific set of commands.
/// Except the ping, login and get me commands, all the other commands require authentication.
#[async_trait]
pub trait Client:
    SystemClient + UserClient + PersonalAccessTokenClient + Sync + Send + Debug
{
    /// Connect to the server. Depending on the selected transport and provided configuration it might also perform authentication, retry logic etc.
    /// If the client is already connected, it will do nothing.
    async fn connect(&self) -> Result<(), Error>;

    /// Disconnect from the server. If the client is not connected, it will do nothing.
    async fn disconnect(&self) -> Result<(), Error>;
}

/// This trait defines the methods to interact with the system module.
#[async_trait]
pub trait SystemClient {
    /// Get the stats of the system such as PID, memory usage, streams count etc.
    ///
    /// Authentication is required, and the permission to read the server info.
    async fn get_stats(&self, command: &GetStats) -> Result<Stats, Error>;
    /// Get the info about the currently connected client (not to be confused with the user).
    ///
    /// Authentication is required.
    async fn get_me(&self, command: &GetMe) -> Result<ClientInfoDetails, Error>;
    /// Get the info about a specific client by unique ID (not to be confused with the user).
    ///
    /// Authentication is required, and the permission to read the server info.
    async fn get_client(&self, command: &GetClient) -> Result<ClientInfoDetails, Error>;
    /// Get the info about all the currently connected clients (not to be confused with the users).
    ///
    /// Authentication is required, and the permission to read the server info.
    async fn get_clients(&self, command: &GetClients) -> Result<Vec<ClientInfo>, Error>;
    /// Ping the server to check if it's alive.
    async fn ping(&self, command: &Ping) -> Result<(), Error>;
}

/// This trait defines the methods to interact with the user module.
#[async_trait]
pub trait UserClient {
    /// Get the info about a specific user by unique ID or username.
    ///
    /// Authentication is required, and the permission to read the users, unless the provided user ID is the same as the authenticated user.
    async fn get_user(&self, command: &GetUser) -> Result<UserInfoDetails, Error>;
    /// Get the info about all the users.
    ///
    /// Authentication is required, and the permission to read the users.
    async fn get_users(&self, command: &GetUsers) -> Result<Vec<UserInfo>, Error>;
    /// Create a new user.
    ///
    /// Authentication is required, and the permission to manage the users.
    async fn create_user(&self, command: &CreateUser) -> Result<(), Error>;
    /// Delete a user by unique ID or username.
    ///
    /// Authentication is required, and the permission to manage the users.
    async fn delete_user(&self, command: &DeleteUser) -> Result<(), Error>;
    /// Update a user by unique ID or username.
    ///
    /// Authentication is required, and the permission to manage the users.
    async fn update_user(&self, command: &UpdateUser) -> Result<(), Error>;
    /// Update the permissions of a user by unique ID or username.
    ///
    /// Authentication is required, and the permission to manage the users.
    async fn update_permissions(&self, command: &UpdatePermissions) -> Result<(), Error>;
    /// Change the password of a user by unique ID or username.
    ///
    /// Authentication is required, and the permission to manage the users, unless the provided user ID is the same as the authenticated user.
    async fn change_password(&self, command: &ChangePassword) -> Result<(), Error>;
    /// Login a user by username and password.
    async fn login_user(&self, command: &LoginUser) -> Result<IdentityInfo, Error>;
    /// Logout the currently authenticated user.
    async fn logout_user(&self, command: &LogoutUser) -> Result<(), Error>;
}

/// This trait defines the methods to interact with the personal access token module.
#[async_trait]
pub trait PersonalAccessTokenClient {
    /// Get the info about all the personal access tokens of the currently authenticated user.
    async fn get_personal_access_tokens(
        &self,
        command: &GetPersonalAccessTokens,
    ) -> Result<Vec<PersonalAccessTokenInfo>, Error>;
    /// Create a new personal access token for the currently authenticated user.
    async fn create_personal_access_token(
        &self,
        command: &CreatePersonalAccessToken,
    ) -> Result<RawPersonalAccessToken, Error>;
    /// Delete a personal access token of the currently authenticated user by unique token name.
    async fn delete_personal_access_token(
        &self,
        command: &DeletePersonalAccessToken,
    ) -> Result<(), Error>;
    /// Login the user with the provided personal access token.
    async fn login_with_personal_access_token(
        &self,
        command: &LoginWithPersonalAccessToken,
    ) -> Result<IdentityInfo, Error>;
}
