use crate::infrastructure::error::Error;
use crate::models::bytes_serializable::BytesSerializable;
use crate::models::command::CommandPayload;
use crate::models::validatable::Validatable;
use bytes::Bytes;
use serde::{Deserialize, Serialize};
use std::fmt::Display;

/// `LogoutUser` command is used to logout the authenticated user.
/// It has no additional payload.
#[derive(Debug, Default, Serialize, Deserialize, PartialEq)]
pub struct LogoutUser {}

impl CommandPayload for LogoutUser {}

impl Validatable<Error> for LogoutUser {
    fn validate(&self) -> Result<(), Error> {
        Ok(())
    }
}

impl BytesSerializable for LogoutUser {
    fn as_bytes(&self) -> Bytes {
        Bytes::new()
    }

    fn from_bytes(bytes: Bytes) -> Result<LogoutUser, Error> {
        if !bytes.is_empty() {
            return Err(Error::InvalidCommand);
        }

        let command = LogoutUser {};
        command.validate()?;
        Ok(command)
    }
}

impl Display for LogoutUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_be_serialized_as_empty_bytes() {
        let command = LogoutUser {};
        let bytes = command.as_bytes();
        assert!(bytes.is_empty());
    }

    #[test]
    fn should_be_deserialized_from_empty_bytes() {
        let command = LogoutUser::from_bytes(Bytes::new());
        assert!(command.is_ok());
    }

    #[test]
    fn should_not_be_deserialized_from_empty_bytes() {
        let command = LogoutUser::from_bytes(Bytes::from_static(&[0]));
        assert!(command.is_err());
    }
}
