use serde::{Deserialize, Serialize};

use crate::utils::byte_size::IggyByteSize;

/// `Stats` represents the statistics and details of the server and running process.
#[derive(Debug, Serialize, Deserialize)]
pub struct Stats {
    /// The unique identifier of the process.
    pub process_id: u32,
    /// The CPU usage of the process.
    pub cpu_usage: f32,
    /// The memory usage of the process.
    pub memory_usage: IggyByteSize,
    /// The total memory of the system.
    pub total_memory: IggyByteSize,
    /// The available memory of the system.
    pub available_memory: IggyByteSize,
    /// The run time of the process.
    pub run_time: u64,
    /// The start time of the process.
    pub start_time: u64,
    /// The total number of bytes read.
    pub read_bytes: IggyByteSize,
    /// The total number of bytes written.
    pub written_bytes: IggyByteSize,
    // / The total size of the messages in bytes.
    // pub messages_size_bytes: IggyByteSize,
    // /// The total number of streams.
    // pub streams_count: u32,
    // /// The total number of topics.
    // pub topics_count: u32,
    // /// The total number of partitions.
    // pub partitions_count: u32,
    // /// The total number of segments.
    // pub segments_count: u32,
    // /// The total number of messages.
    // pub messages_count: u64,
    /// The total number of connected clients.
    pub clients_count: u32,
    // /// The total number of consumer groups.
    // pub consumer_groups_count: u32,
    /// The name of the host.
    pub hostname: String,
    /// The details of the operating system.
    pub os_name: String,
    /// The version of the operating system.
    pub os_version: String,
    /// The version of the kernel.
    pub kernel_version: String,
}

// use xitca_web::{error::Error, handler::Responder, http::WebResponse, WebContext};

// impl<'r, C, B> Responder<WebContext<'r, C, B>> for Stats {
//     type Response = WebResponse;
//     type Error = Error<C>;
//     async fn respond(self, ctx: WebContext<'r, C, B>) -> Result<Self::Response, Self::Error> {
//         // logic for generating response from Stats
//         todo!()
//     }
// }
