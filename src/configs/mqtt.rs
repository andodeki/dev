use crate::utils::duration::IggyDuration;
use byte_unit::Byte;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use serde_with::DisplayFromStr;

#[serde_as]
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct MqttConfig {
    pub enabled: bool,
    pub broker_address: String,
    pub port: u16,
    pub username: String,
    pub password: String,
    #[serde_as(as = "DisplayFromStr")]
    pub keep_alive_interval: IggyDuration,
    #[serde_as(as = "DisplayFromStr")]
    pub max_idle_timeout: IggyDuration,
    pub certificate: MqttCertificateConfig,
}

// transport: Transport,
// keep_alive: Duration,
// clean_session: bool,
// client_id: String,
// credentials: Option<(String, String)>,
// max_incoming_packet_size: usize,
// max_outgoing_packet_size: usize,
// request_channel_capacity: usize,
// max_request_batch: usize,
// pending_throttle: Duration,
// inflight: u16,
// last_will: Option<LastWill>,
// manual_acks: bool,
// #[default("localhost")]
// mqtt_host: &'static str,
// #[default("")]
// mqtt_user: &'static str,
// #[default("")]
// mqtt_pass: &'static str,

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct MqttCertificateConfig {
    pub self_signed: bool,
    pub cert_file: String,
    pub key_file: String,
}
